!
!
!
! This file contains the resource text strings for the IBM Linux 3270 and 5250
! emulators, in particular the main emulator windows.

! This resource file will be stored on all servers in locale specific ASCII (819 for Latin1
! languages). 

! Resource file format:
! 1. Standard ASCII text file.
! 2. Comment lines are preceded by a "!" character in column 1 (these lines not translated).
! 3. This resource file contains text strings for the 3270 and 5250 emulators.  Much of
!    the resource text is common to both emulators. Some is specific to one emulator.
!    IBM9999 = Resources used by both 3270 and 5250 emulators.
!    IBM3270 = Resources used by the 3270 emulator.
!    IBM5250 = Resources used by the 5250 emulator.
! 4. Resource format: 
!    - A resource line starts with IBM3270*, IBM5250*, or IBM9999*, followed by a resource name
!      and a ":".  Do NOT make any changes to the text to the left of the first ":"
!    - Text to the right of the first ":" on a line is translated.

!******************************************************************************
!************************ BEGIN DO NOT TRANSLATE ******************************
!******************************************************************************
!*   The first section of resources are are locale specific but               *
!*   DO NOT REQUIRE TRANSLATION.                                              *
!*                                                                            *
!* This source file enables the resource section for Latin1 and Latin2.       *
!*                                                                            *
!* For ru_RU or Latin5 locales and DBCS locales                               *
!*      1. Place a '!' as the first character in each line in the Latin1      *
!*      and Latin2 section                                                    *
!*      2. Remove the '!' character for each resource line in the apropriate  *
!*      section that matches your locale.                                     *
!*                                                                            *
!* For example, ru_RU should place ! in front of all lines in the LATIN1      *
!* section remove the ! from all lines in the LATIN5 section.                 *      
!******************************************************************************

!**************************************************************************
!****************   BEGIN LATIN1 and LATIN2                ****************
!**************************************************************************
IBM9999*font_List:     !-*-*-medium-r-normal-*-*-*-*-*-m-150-*-*;-*-*-medium-r-normal-*-*-*-*-*-m-110-*-*;-*-*-medium-r-normal-*-*-*-*-*-m-90-*-*;-*-*-medium-r-normal-*-*-*-*-*-m-70-*-*;-*-*-medium-r-normal-*-*-*-*-*-m-60-*-*
IBM9999*largestFontList:     -*-*-medium-r-normal-*-*-*-*-*-m-150-*-*
IBM9999*extraLargeFontList:  -*-*-medium-r-normal-*-*-*-*-*-m-110-*-*
IBM9999*largeFontList:       -*-*-medium-r-normal-*-*-*-*-*-m-90-*-*
IBM9999*mediumFontList:      -*-*-medium-r-normal-*-*-*-*-*-m-70-*-*
IBM9999*smallFontList:       -*-*-medium-r-normal-*-*-*-*-*-m-60-*-*
!**************************************************************************
!****************   END LATIN1 and LATIN2                  ****************
!**************************************************************************

!**************************************************************************
!****************   BEGIN LATIN5                           ****************
!**************************************************************************
!IBM9999*font_List:     -*-*-medium-r-normal-*-*-*-*-*-c-100-*-*;-*-*-medium-r-normal-*-*-*-*-*-c-90-*-*;-*-*-medium-r-normal-*-*-*-*-*-c-70-*-*;-*-*-medium-r-normal-*-*-*-*-*-c-60-*-*
!IBM9999*largestFontList:     -*-*-medium-r-normal-*-*-*-*-*-c-100-*-*
!IBM9999*extraLargeFontList:  -*-*-medium-r-normal-*-*-*-*-*-c-100-*-*
!IBM9999*largeFontList:       -*-*-medium-r-normal-*-*-*-*-*-c-90-*-*
!IBM9999*mediumFontList:      -*-*-medium-r-normal-*-*-*-*-*-c-70-*-*
!IBM9999*smallFontList:       -*-*-medium-r-normal-*-*-*-*-*-c-60-*-*
!**************************************************************************
!****************   END   LATIN5                           ****************
!**************************************************************************

!**************************************************************************
!****************   BEGIN DBCS (ja_JP ko_KR zh_CN)   ****************
!**************************************************************************
!IBM9999*font_List:            -*-*-medium-r-normal--24-*-*-*-c-*;-*-*-medium-r-normal--16-*-*-*-c-*
!IBM9999*largestFontList:     -*-*-medium-r-normal--24-*-*-*-c-*
!IBM9999*extraLargeFontList:  -*-*-medium-r-normal--24-*-*-*-c-*
!IBM9999*largeFontList:       -*-*-medium-r-normal--16-*-*-*-c-*
!IBM9999*mediumFontList:      -*-*-medium-r-normal--16-*-*-*-c-*
!IBM9999*smallFontList:       -*-*-medium-r-normal--16-*-*-*-c-*
!**************************************************************************
!****************   END DBCS (ja_JP ko_KR zh_CN)     ****************
!**************************************************************************

!**************************************************************************
!****************   BEGIN DBCS (zh_TW)   ****************
!**************************************************************************
!IBM9999*font_List:            -*-*-medium-r-normal--24-*-*-*-c-*,-arphic-ar pl mingti2l big5-medium-r-normal--24-240-0-0-c-*;-*-*-medium-r-normal--16-*-*-*-c-*,-arphic-ar pl mingti2l big5-medium-r-normal--16-160-0-0-c-*
!IBM9999*largestFontList:     -*-*-medium-r-normal--24-*-*-*-c-*,-arphic-ar pl mingti2l big5-medium-r-normal--24-240-0-0-c-*
!IBM9999*extraLargeFontList:  -*-*-medium-r-normal--24-*-*-*-c-*,-arphic-ar pl mingti2l big5-medium-r-normal--24-240-0-0-c-*
!IBM9999*largeFontList:       -*-*-medium-r-normal--16-*-*-*-c-*,-arphic-ar pl mingti2l big5-medium-r-normal--16-160-0-0-c-*
!IBM9999*mediumFontList:      -*-*-medium-r-normal--16-*-*-*-c-*,-arphic-ar pl mingti2l big5-medium-r-normal--16-160-0-0-c-*
!IBM9999*smallFontList:       -*-*-medium-r-normal--16-*-*-*-c-*,-arphic-ar pl mingti2l big5-medium-r-normal--16-160-0-0-c-*
!**************************************************************************
!****************   END DBCS (zh_TW)     ****************
!**************************************************************************

!uncomment the appropriate inputMethod
!For LatinX (non DBCS locales) or locales without an input method.
IBM5250*inputMethod:
!
! For ja_JP
!IBM5250*inputMethod: kinput2
!
! For ko_KR 
!IBM5250*inputMethod: Ami
!
!For zh_CN
!IBM5250*inputMethod: xcin-zh_CN
!
!For zh_TW
!IBM5250*inputMethod: xcin-zh_TW

!**************************************************************************
!****************   END DO NOT TRANSLATE                   ****************
!**************************************************************************



IBM5250*MainWindow.title: 5250
IBM3270*MainWindow.title: 3270

IBM5250*errorMsgDialog_popup.title: 5250 エラー
IBM3270*errorMsgDialog_popup.title: 3270 エラー

IBM5250*warningMsgDialog_popup.title: 5250 情報
IBM3270*warningMsgDialog_popup.title: 3270 情報

IBM5250*hostNameDialog_popup.title: 新規 5250 セッション
IBM3270*hostNameDialog_popup.title: 新規 3270 セッション

IBM5250*hostNameDialog.selectionLabelString: 新規 5250 ホスト名を入力してください。
IBM3270*hostNameDialog.selectionLabelString: 新規 3270 ホスト名を入力してください。

IBM5250*hostNameDialog.iconName: 新規 5250 セッション
IBM3270*hostNameDialog.iconName: 新規 3270 セッション

IBM5250*lowMemoryDialog_popup.title: 5250 低位メモリー
IBM3270*lowMemoryDialog_popup.title: 3270 低位メモリー

IBM5250*new5250Button.labelString: 新規 5250...

IBM5250*quitButton.labelString: 5250 の終了
IBM3270*quitButton.labelString: 3270 の終了

IBM5250*localPrintControls_popup.title: 5250 画面印刷
IBM3270*localPrintControls_popup.title: 3270 画面印刷


!**************************************************************************************
!** Resources in both 5250 and 3270 that do not need separate translations
!**************************************************************************************

!***********************
!**  Various Dialogs  **
!***********************
IBM9999*lowMemoryDialog.messageString: 操作を完了するにはメモリーが不足しています。

IBM9999*ErrorMsgMaxSessions: セッションは許可されません。 セッションの最大数を超えました。

! -DISPLAY_NAME is not translated.
IBM9999*ErrorMsgInvalidName: -DISPLAY_NAME パラメーターの仕様が無効です。

! \n in the following resource string allows text following \n to appear on a new line.
! \n is not translated. You can move, add, or remove the \n as needed.
IBM9999*ErrorMsgNameInUse: -DISPLAY_NAME パラメーターに指定された 1 つまたは複数の名前はすでに使用されています。\n\
システム管理者がセッションの数を制限しています。

! BROWSER is not translated.
IBM9999*ErrorMsgBrowserStart: BROWSER 環境変数が定義されていません。ブラウザーを開始できません。

IBM9999*ErrorMsgTelnetUndefined: Telnet がサービス・ファイルに定義されていません...

IBM9999*ErrorMsgUnknownHost: ホストが不明です。

IBM9999*ErrorMsgUnableToConnect: 接続を開始することができません。

IBM9999*ErrorMsgTelnetNegoFailed: Telnet の折衝に失敗しました。

IBM9999*ErrorMsgRetryButton: 再試行

! \n in the following resource string allows text following \n to appear on a new line.
IBM9999*ErrorMsgNoHostData: ホストへの接続が失われました。\n\
エミュレーター・セッションを再始動してみてください。

! \n in the following resource string allows text following \n to appear on a new line.
IBM9999*ErrorMsgNoHostData2: ホストへの接続が失われました。\n\
エミュレーター・セッションの開始をやり直しますか？

IBM9999*ErrorMsgFatalTelnetError: 致命的な Telnet エラーが発生しました。

IBM9999*ErrorMsgColorMapActive: カラー・マップ・アプリケーションはすでに別のセッションでアクティブになっています。

IBM9999*ErrorMsgColorMapLowMemory: カラー・マッピングを開始するにはメモリーが不足しています。

IBM9999*ErrorMsgKeyMapActive: キー・マップ・アプリケーションはすでに別のセッションでアクティブになっています。

! this cannot be made much longer during translation.
IBM9999*ErrorMsgKeypadActive:  キーパッド・カスタマイザー・アプリケーションはすでに別のセッションでアクティブになっています。

IBM9999*ErrorMsgKeyMapLowMemory: キーボード再マップを開始するにはメモリーが不足しています。

IBM9999*ErrorMsgKeypadLowMemory:   キーパッド・カスタマイザーを開始するにはメモリーが不足しています。

IBM9999*ErrorMsgHelpViewLowMemory: ヘルプ・ビューアーを開始するにはメモリーが不足しています。

IBM9999*ErrorMsgMiscellaneousLowMemory: 各種設定の変更を開始するにはメモリーが不足しています。

! this cannot be made any longer during translation.
IBM9999*ErrorMsgNSMServerDownLevel:   記録できません。 IBM Network Station Manager サーバーが下位レベルにあります。

IBM9999*ErrorMsgNoPrinters:           構成済みのプリンターはありません。 システム管理者にご相談ください。

! \n in the following resource string allows text following \n to appear on a new line.
IBM9999*ErrorMsgNoKeypadSelected: ファイルが選択されていません。\n\
ファイルを選択するか、あるいは「取消」を選択してください。 

! \n in the following resource string allows text following \n to appear on a new line.
IBM9999*ErrorMsgNoKeypads: キーパッド・ファイルが見つかりません。 「キーパッドのカスタマイズ...」オプションを\n\
オプション・メニューから選択して、ユーザー・キーパッドを作成してください。

IBM9999*ErrorMsgTooManyKeypads: 一度にアクティブにできるのは 10 キーパッドだけです。
	
IBM9999*ErrorMsgFileNameChars: ファイル名として入力した文字は無効です。

! \n in the following resource string allows text following \n to appear on a new line.
IBM9999*WarningMsgColorNotAvailable: 一部のエミュレーター・カラーが使用できません。\n\
これらのカラーは、ユーザーのセッションに合わせて調整されます。

IBM9999*WarningMsgColorFileCorrupt: カラー・ファイルのエラーが存在しており、デフォルト・カラーが使用されます。

IBM9999*hostNameDialog*startupPlaybackChoice.labelString: 始動再生ファイルの使用

! java ECL that is not supported yet but please translate
IBM9999*runAppletDialog_popup.title: ECL Java アプレット

! java ECL that is not supported yet but please translate
IBM9999*runAppletDialog.selectionLabelString: クラス名を入力してください。

! java ECL that is not supported yet but please translate
IBM9999*runAppletDialog.iconName: ECL Java アプレット

! java ECL that is not supported yet but please translate
IBM9999*runAppletDialog: クラス名を入力してください。

! java ECL that is not supported yet but please translate
IBM9999*ErrorMsgUnableToRunApplet: クラスをロードすることができません


!*******************************
!**  Menu items in emulators  **
!*******************************
IBM9999*cmdCascade.labelString: コマンド

IBM9999*editCascade.labelString: 編集

IBM9999*pasteButton.labelString: 貼り付け

IBM9999*copyButton.labelString: コピー

IBM9999*cutButton.labelString: 切り取り

IBM9999*optCascade.labelString: オプション

IBM9999*fontMenuCascade.labelString: フォント

IBM9999*pfkeysButton.labelString: キーパッド...

IBM9999*keypadCascade.labelString: キーパッド

IBM9999*customizeButton.labelString: キーパッドのカスタマイズ...   

IBM9999*recordButton.labelString: 記録...

IBM9999*playbackButton.labelString: 再生...

IBM9999*miscButton.labelString: 各種設定の変更...

IBM9999*colormapButton.labelString: カラー・マッピング...

IBM9999*keymapButton.labelString: キーボード再マップ...

! java ECL that is not supported yet but please translate
IBM9999*runAppletButton.labelString: アプレットの実行...

IBM9999*ctlCascade.labelString: 制御

IBM9999*resetButton.labelString: リセット・キー

IBM9999*sysreqButton.labelString: システム要求キー

IBM9999*attentionButton.labelString: アテンション・キー

IBM9999*helpkeyButton.labelString: ヘルプ・キー

IBM9999*enterkeyButton.labelString: Enter キー

IBM9999*prtCascade.labelString: 印刷

IBM9999*systemButton.labelString: システム

IBM9999*localButton.labelString: 画面印刷...

IBM9999*helpCascade.labelString: ヘルプ

! message on IBM5250 on status line, cannot be made any longer, you can remove some periods.
IBM9999*msgWaitingMsg.labelString: メッセージ待機中.....

IBM9999*cursorRowCol.labelString:  01/001

IBM9999*okLabelString: 了解

IBM9999*cancelLabelString: 取消

!****************************
!** Local Print Resources
!****************************

IBM9999*localPrintPrinterSelectButton.labelString: プリンターの選択:

IBM9999*localPrintPrinterSelectControls_popup.title: プリンター・セレクター

IBM9999*localPrintPrinterSelectServerDelineator: @

IBM9999*localPrintPrinterSelectLocalDesignator: (ローカル)

IBM9999*localPrintPDTLabel.labelString: プリンター定義テーブル:

IBM9999*localPrintOtherOptions.labelString: その他の印刷オプション:

IBM9999*localPrintFormFeedLabel.labelString: 用紙送り:

IBM9999*localPrintFormFeedYesButton.labelString: はい

IBM9999*localPrintFormFeedNoButton.labelString: いいえ

IBM9999*localPrintOrientationLabel.labelString: 方向付け:

IBM9999*localPrintOrientationPortraitButton.labelString: 縦長

IBM9999*localPrintOrientationLandscapeButton.labelString: 横長

IBM9999*localPrintControls*startBtn.labelString: 印刷

IBM9999*localPrintControls*cancelBtn.labelString: 取消

IBM9999*localPrintControls*selectOptions.labelString: 印刷オプションの選択

IBM9999*localPrintControls*printing.labelString: 印刷中...

IBM9999*PrintCommand.labelString: 印刷コマンド


!********************************************************
!**                  Record Resources
!**
!** MAX:  Push buttons should not exceed 8 characters
!********************************************************
IBM9999*recordControl_popup.title: 記録の制御

IBM9999*recordControl*startBtn.labelString: 開始

IBM9999*recordControl*cancelBtn.labelString: 取消

IBM9999*recordControl*pauseBtn.labelString: 一時停止

IBM9999*recordControl*stopBtn.labelString: 停止

IBM9999*recordControl*recordControlLabelRecording.labelString: 記録中。

IBM9999*recordControl*recordControlLabelPaused.labelString: 一時停止されています。

IBM9999*recordControl*recordControlKeysRemaingLabel.labelString: 残りのスペース:

IBM9999*recordPauseControls_popup.title: 記録一時停止オプション

! this cannot be made much longer during translation. 
IBM9999*recordPauseControls*recordPauseChoicesInstructions.labelString: 記録は一時停止されています。 再生中に実行するオプションを選択してください。

IBM9999*recordPauseControls*recordPauseRecordBtn.labelString: この時点で再生の一時停止

IBM9999*recordPauseControls*recordPauseUseridBtn.labelString: この時点でユーザー ID の挿入

IBM9999*recordPauseControls*recordPausePasswordBtn.labelString: この時点でパスワードの挿入

IBM9999*recordPauseControls*recordPauseNoneBtn.labelString: なし

IBM9999*recordPauseControls*continueBtn.labelString:   記録の続行

IBM9999*recordPauseControls*pauseStopBtn.labelString: 記録の停止

IBM9999*recordFileControl_popup.title: 再生ファイルの保管の制御

IBM9999*recordFileControl*recordFileListLabel.labelString: 置き換える再生ファイルを選択してください。

IBM9999*recordFileControl*acceleratorBtn.labelString: オプション・アクセラレーターの割り当て

IBM9999*recordFileControl*accButton0.labelString: なし

! Function keys for accelerators.
IBM9999*recordFileControl*accButton1.labelString:  F1

IBM9999*recordFileControl*accButton2.labelString:  F2

IBM9999*recordFileControl*accButton3.labelString:  F3

IBM9999*recordFileControl*accButton4.labelString:  F4

IBM9999*recordFileControl*accButton5.labelString:  F5

IBM9999*recordFileControl*accButton6.labelString:  F6

IBM9999*recordFileControl*accButton7.labelString:  F7

IBM9999*recordFileControl*accButton8.labelString:  F8

IBM9999*recordFileControl*accButton9.labelString:  F9

IBM9999*recordFileControl*accButton10.labelString:  F10

IBM9999*recordFileControl*accButton11.labelString:  F11

IBM9999*recordFileControl*accButton12.labelString:  F12

IBM9999*recordFileControl*accButton13.labelString:  F13

IBM9999*recordFileControl*accButton14.labelString:  F14

IBM9999*recordFileControl*accButton15.labelString:  F15

IBM9999*recordFileControl*accButton16.labelString:  F16

IBM9999*recordFileControl*accButton17.labelString:  F17

IBM9999*recordFileControl*accButton18.labelString:  F18

IBM9999*recordFileControl*accButton19.labelString:  F19

IBM9999*recordFileControl*accButton20.labelString:  F20

IBM9999*recordFileControl*accButton21.labelString:  F21

IBM9999*recordFileControl*accButton22.labelString:  F22

IBM9999*recordFileControl*accButton23.labelString:  F23

IBM9999*recordFileControl*accButton24.labelString:  F24

IBM9999*recordFileControl*recordFileTextLabel.labelString: 新規再生ファイル名を入力してください。

IBM9999*recordFileControl*saveBtn.labelString: 保管

IBM9999*recordFileControl*doNotSaveBtn.labelString: 保管しない

IBM9999*recordFileControl*recordFileReplaceWarningDialog_popup.title: 再生ファイルの置き換えの警告

IBM9999*recordFileControl*recordFileReplaceWarningDialog.messageString: ファイルはすでに存在しています。 上書きしますか？

IBM9999*recordFileControl*recordFileMsgDialog_popup.title: 再生ファイルの保管エラー・メッセージ

IBM9999*RecordFileMsgNoOpen: ファイルをオープンすることができません。  キー・ストロークは保管されていません。

IBM9999*RecordFileMsgNoAccess: ディレクトリーにアクセスすることができません。

IBM9999*RecordFileMsgNoMake: ディレクトリーを作成することができません。


!****************************
!** Playback Resources
!****************************
IBM9999*playbackControl_popup.title: 再生の制御

IBM9999*playbackControl*playbackControlLabelSelectFile.labelString: 再生する再生ファイルを選択してください。

IBM9999*playbackControl*playbackFileListLabel.labelString: ユーザー再生ファイル

IBM9999*playbackControl*playbackDefaultFileListLabel.labelString: デフォルトの再生ファイル

IBM9999*scaleBtn.titleString: 再生速度

IBM9999*playbackControl*startBtn.labelString: 開始

IBM9999*playbackControl*cancelBtn.labelString: 取消

IBM9999*playbackControl*deleteBtn.labelString: 削除

IBM9999*playbackControl*editBtn.labelString: 編集

IBM9999*playbackControl*pauseBtn.labelString: 一時停止

IBM9999*playbackControl*continueBtn.labelString: 続行

IBM9999*playbackControl*stopBtn.labelString: 停止

IBM9999*playbackControl*playbackControlLabelStopped.labelString: 停止しました。

IBM9999*playbackControl*playbackControlLabelPlaying.labelString: 再生中。

IBM9999*playbackControl*playbackControlLabelPaused.labelString:  一時停止されています。

IBM9999*playbackControl*playbackControlLabelDone.labelString: 完了しました。

IBM9999*playbackControl*playbackKeysRemainingLabel.labelString:  残りのデータ:

IBM9999*playbackControl*playbackDeleteWarningDialog_popup.title: 再生の削除の警告

IBM9999*playbackControl*playbackDeleteWarningDialog.messageString: 「了解」で選択した 1 つまたは複数のファイルを削除しますか？

IBM9999*playbackControl*playbackMsgDialog_popup.title: 再生エラー・メッセージ

IBM9999*playbackControl*playbackPauseDialog_popup.title: 再生一時停止

IBM9999*playbackControl*playbackPauseDialog.okLabelString:        続行

IBM9999*playbackControl*playbackPauseDialog.cancelLabelString:    停止

IBM9999*playbackPauseDialogMsg1: 再生が一時停止しました。 データを入力して続行してください。

IBM9999*playbackPauseDialogMsg2: 再生が一時停止しました。 パスワードを入力して続行してください。

IBM9999*playbackPauseDialogMsg3: ユーザーが再生を一時停止しました。

IBM9999*PlaybackMsgNoStartup: 始動再生ファイルが再生リストにありません。

IBM9999*PlaybackMsgFileNotInList:  再生ファイルが再生リストにありません。

IBM9999*PlaybackMsgFileCorrupt: 再生ファイルが「記録」によって作成されていません。

IBM9999*PlaybackMsgFileNoRead: キーとなる再生ファイルを読み取ることができません。 再生を開始することができません。

IBM9999*PlaybackMsgFileNoOpen: 再生ファイルをオープンすることができません。 再生を開始することができません。

IBM9999*PlaybackMsgIncompatible:  再生ファイルには現行画面との互換性がありません。

IBM9999*PlaybackMsgPasswordLocation:  パスワードをカーソル位置で再生することができません。


!*****************************
! Keypad Resources
!*****************************
IBM9999*keypadfilechangebox_popup.title:   ポップアップ・キーパッド

IBM9999*keypadfilechangeboxlabel.labelString:   キーパッド・ファイルの選択:

IBM9999*userKeypads.labelString:  ユーザー・キーパッド

IBM9999*defaultKeypads.labelString:  デフォルト・キーパッド

IBM9999*cancelkeypad.labelString:   取消

IBM9999*keypadOKbox.labelString:   了解

!*****************************************************************************
! This menubar resource is used for double byte languages, but DOES need to be
! translated into all MRI languages (because users can have a different MRI
! language than their emulator session language).
!*****************************************************************************
IBM9999*altviewButton.labelString:  代替ビュー

!***********************************************************
! The resources below are used to prompt the user to choose between two types 
! of Japanese.  These resources should be translated into all languages.
!***********************************************************
IBM9999*hostNameDialog.helpLabelString:  拡張...
IBM9999*katawidget.title:   日本語ホストのコード・ページの選択
IBM9999*labelw.labelString:  ホストのコード・ページの選択
IBM9999*button_two.labelString:  拡張カタカナ (5026)
IBM9999*button_one.labelString:  拡張ローマ字 (5035)
IBM9999*okw.labelString:  了解
IBM9999*cancelw.labelString:  取消


!***********************************
!* Shared Emulator Print Resources *
!***********************************

IBM9999*localPrintPrintSelectionLabel.labelString:       印刷選択:

IBM9999*localPrintPrintSelectionTextButton.labelString:  テキスト

IBM9999*localPrintPrintSelectionImageButton.labelString: イメージ

IBM9999*localPrintImageColorSupportLabel.labelString: カラー印刷:

IBM9999*localPrintImageColorSupportFalseButton.labelString: いいえ

IBM9999*localPrintImageColorSupportTrueButton.labelString: はい

IBM9999*localPrintPageSizeMenuLabel.labelString:  ページ・サイズ:

! The following are printer page sizes.
IBM9999*localPrintPageSizeMenu*pageSize1.labelString:  レター (8.5 x 11 インチ)

IBM9999*localPrintPageSizeMenu*pageSize2.labelString:  リーガル (8.5 x 14 インチ)

IBM9999*localPrintPageSizeMenu*pageSize3.labelString:  A4 (210 x 297 mm)

IBM9999*localPrintPageSizeMenu*pageSize4.labelString:  A5 (148 x 210 mm)

IBM9999*localPrintPageSizeMenu*pageSize5.labelString:  B5 ISO (176 x 250 mm)

IBM9999*localPrintPageSizeMenu*pageSize6.labelString:  B5 JIS (182 X 257 mm)

IBM9999*localPrintPageSizeMenu*pageSize7.labelString:  エグゼクティブ (7.25 x 10.5 インチ)

IBM9999*localPrintPageSizeMenu*pageSize8.labelString:  ステートメント (8.5 x 5.5 インチ)

IBM9999*localPrintDPIMenuLabel.labelString:  レゾリューション (dpi):

IBM9999*localPrintDPIMenu*DPI1.labelString:  75

IBM9999*localPrintDPIMenu*DPI2.labelString:  100

IBM9999*localPrintDPIMenu*DPI3.labelString:  150

IBM9999*localPrintDPIMenu*DPI4.labelString:  200

IBM9999*localPrintDPIMenu*DPI5.labelString:  300

IBM9999*localPrintDPIMenu*DPI6.labelString:  600

IBM9999*ErrorMsgBrowserLowMemory:  操作を完了するにはメモリーが不足しています。

IBM9999*userKeypads.labelString: ユーザー・キーパッド

IBM9999*defaultKeypads.labelString: デフォルト・キーパッド

IBM9999*playbackControl*playbackDeleteWarningDialog.cancelLabelString: 取消

IBM9999*playbackControl*playbackDeleteWarningDialog.okLabelString: 了解

!***********************************
!**  Resources only used by 3270  **
!***********************************
! 'Command' menu
IBM3270*sessMenuCascade.labelString: 新規 3270 セッション

IBM3270*new_sessionButton.labelString: 新規 3270 セッション

IBM3270*new_mod2Button.labelString: 新規 24x80 セッション

IBM3270*new_mod3Button.labelString: 新規 32x80 セッション

IBM3270*new_mod4Button.labelString: 新規 43x80 セッション

IBM3270*new_mod5Button.labelString: 新規 27x132 セッション

IBM3270*graphicsMsg.labelString: グラフィックス

IBM3270*font0Button.labelString: デフォルト

IBM3270*printTelnetFailure: 印刷\\nTelnet の折衝に失敗しました。

!**************************************************
!*
!*        Printing translations 
!*
!*      Added by AJMEYER for V310.nc
!*
!**************************************************

IBM9999*printDialogPrinterTypeLabel.labelString:  プリンター・タイプ:

IBM9999*printDialogPrinterTypeMenu*postscript.labelString:  PostScript

IBM9999*printDialogPrinterTypeMenu*ascii.labelString:  ASCII

IBM9999*printDialogPrinterTypeMenu*pcl.labelString:  PCL

IBM9999*printDialogPrinterSelectControls_popup.title:   プリンター・セレクター

IBM9999*printSelectDialogOKButton.labelString:  了解

IBM9999*printSelectDialogCancelButton.labelString:  取消

IBM9999*printDialogSelectPrinter.labelString: プリンターの選択:

IBM9999*printDialogPrintCommandLabel.labelString: 印刷コマンド:

IBM5250*hostNameDialog*useridLabel.labelString: ユーザー ID:

IBM5250*hostNameDialog*passwordLabel.labelString: パスワード:

!**************************************************
!*
!*                ICU path  
!*
!*     
!**************************************************


IBM5250*ICUTablePath:  /usr/local/share/icu/3.4
! ENGL1SH_VERS10N 1.8.2.1 DO NOT REMOVE OR CHANGE THIS LINE
