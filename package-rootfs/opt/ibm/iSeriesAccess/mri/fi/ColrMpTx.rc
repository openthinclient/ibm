!
!
!
! This file contains the resource text strings for the IBM Network Station 3270 and 5250
! emulators, in particular the color mapping program.

! This resource file will be stored on all servers in locale specific ASCII (819 for Latin1
! languages). This resource file is stored on the booting server as N3_5ColM.

! Resource file format:
! 1. Standard ASCII text file.
! 2. Comment lines are preceded by a "!" character in column 1 (these lines not translated).
! 3. Resource format: 
!    - A resource line starts with ColorMap* followed by a resource name
!      and a ":".  Do NOT make any changes to the text to the left of the first ":"
!    - Text to the right of the first ":" on a line is translated.

ColorMap*title: 			V�rien valinta

! icons
ColorMap*iname3270:                     3270-istunnon v�rit
ColorMap*iname5250:                     5250-istunnon v�rit


ColorMap*corrupt:               V�ritiedosto on viallinen

ColorMap*lowMemMsgDialog.messageString:  Muisti ei riit� ohjeen katseluohjelman aloittamiseen.

ColorMap*okLabelString: OK

ColorMap*cancelLabelString: Peruutus

!*******************************************************************************
!**     Preferences area resources (top of window)                            **
!*******************************************************************************
ColorMap*modeInstructions.labelString:  V�rien valintatoiminnon valinta:

ColorMap*Basic.labelString:             Perusasetukset

ColorMap*Advanced.labelString:          Lis�asetukset


!*************************************************************
!**   Basic and advanced action bar area resources          **
!*************************************************************
ColorMap*actionForm.saveButton.labelString:  	Tallennus

ColorMap*actionForm.deleteButton.labelString:   Poisto

ColorMap*actionForm.applyButton.labelString:    Muutosten k�ytt� istunnossa

ColorMap*actionForm.quitButton.labelString:     Lopetus

ColorMap*actionForm.helpButton.labelString:     Ohje


!************************************************
!**  Basic and advanced color savebox          **
!************************************************
ColorMap*saveBoxLabel.labelString:         Tallennusvaihtoehdon valinta:

ColorMap*saveBoxToggle1.labelString:       Tallennus kaikkien istuntojen oletusarvoksi

ColorMap*saveBoxToggle2.labelString:       Tallennus vain t�t� istuntoa varten

ColorMap*saveBoxSaveBtn.labelString:  	   Tallennus

ColorMap*saveBoxCancelBtn.labelString:     Peruutus

ColorMap*advancedSaveBoxLabel.labelString: Lis�v�ritiedoston nimi:

! Characters in file names must be 819 (ISO8859-1)
ColorMap*invalidFileName.value:            Kirjoitettua merkki� ei voi k�ytt�� tiedostonimess�.

ColorMap*invalidFileNameOkbutton.labelString:  OK

!****************************************
!**  Overwrite existing file dialog    **
!****************************************
ColorMap*fileWarningDialog.dialogTitle:        V�rien valinnan varoitus

ColorMap*fileWarningDialog.messageString:      Tiedosto on jo olemassa. Haluatko varmasti korvata sen?

ColorMap*fileWarningDialog.okLabelString:      Kyll�

ColorMap*fileWarningDialog.cancelLabelString:  Ei



!***************************************
!**  Basic color work area            **
!***************************************
ColorMap*basicListLabel.labelString:        V�rimallin valinta:

! The following two resource lines are a late addition to the color mapping resource file (March 12, 1998).  These indicate color map files that came from a system level or user created color map files.
ColorMap*systemLabel:                       J�rjestelm�:

ColorMap*userLabel:                         K�ytt�j�:

ColorMap*ibmBlack:                          IBM: Musta tausta

ColorMap*ibmDarkGrey:                       IBM: Tummanharmaa tausta

ColorMap*ibmLightGrey:                      IBM: Vaaleanharmaa tausta

ColorMap*ibmLight:                          IBM: Vaalea tausta

ColorMap*ibmLightBlue:                      IBM: Vaaleansininen tausta



!******************************************
!**  Advanced color work area            **
!******************************************
ColorMap*advancedListLabel.labelString:     Muutettavan komponentin valinta:

! The following 22 *adv type resources are limited in size; they should not exceed 28 characters.
ColorMap*advMenuBarBack:                    Valikkorivin tausta

ColorMap*advMenuBarFore:                    Valikkorivin edusta

ColorMap*advMainSess:                       P��istunnon tausta

ColorMap*advPushbut:                        Painikkeen tausta

ColorMap*advBlue:                           Sininen teksti

ColorMap*advGreen:                          Vihr. teksti

ColorMap*advPink:                           V.pun. teksti

ColorMap*advRed:                            Pun. teksti

ColorMap*advTurq:                           Turk. teksti

ColorMap*advWhite:                          Valkoinen teksti

ColorMap*advYellow:                         Keltainen teksti

ColorMap*advCursor:                         Kohdistin

ColorMap*advMouse:                          Hiiren osoitin

ColorMap*advRule:                           Kohdistusviiva

ColorMap*advStatLineBack:                   Tilarivin tausta

ColorMap*advStatLineFore:                   Tilarivin edusta

ColorMap*advMonoImageBack:                  Yksiv�risen kuvan tausta

ColorMap*advMonoImageFore:                  Yksiv�risen kuvan edusta

! Note: foreground color used for assist program windows
ColorMap*advAssistFore:                     Apuohjelman edusta

! Note: button background for assist program windows
ColorMap*advAssistForm:                     Apuohjelman tausta

! Note: main background color for assist program windows
ColorMap*advAssistPrimary:                  Apuohjelman p��ikkuna

! Note: entry area background color for assist program windows
ColorMap*advAssistEntry:                    Apuohjelman sy�tt�kentt�


ColorMap*red.titleString:                   PUNAINEN

ColorMap*green.titleString:                 VIHRE�

ColorMap*blue.titleString:                  SININEN

ColorMap*currentColorLabel.labelString:     Nykyinen v�ri

ColorMap*applyCurrentColor.labelString:     Nykyisen v�rin k�ytt�


!************************************
!**  Delete file dialog            **
!************************************
ColorMap*deleteDialog.deleteBoxDeleteBtn.labelString:  Poisto

ColorMap*deleteDialog.deleteBoxCancelBtn.labelString:  Peruutus

! title of delete dialog
ColorMap*deleteDialog.dialogTitle:                     V�ritiedoston poisto

ColorMap*deleteDialog.deleteBoxLabel.labelString:      Poistettavat tiedostot:

ColorMap*deleteDialog.deleteBoxLabelAlt.labelString:   Poistettavia tiedostoja ei ole.

! message at bottom of primary window
ColorMap*selectedDeleted:                              J�rjestelm� on poistanut valitut v�ritiedostot.


!*****************************************************
!**  Delete file verification dialog box            **
!*****************************************************
ColorMap*deleteVerifyDialog.dialogTitle:        Poiston vahvistus

ColorMap*deleteVerifyDialog.okLabelString:      OK

ColorMap*deleteVerifyDialog.cancelLabelString:  Peruutus

! this cannot be much longer
ColorMap*noUndoDelete:                          Tiedostojen poistoa ei voi kumota. Haluatko jatkaa?

ColorMap*noFilesDelete:                         Tiedostoja ei ole valittu. J�rjestelm� ei voi toteuttaa poistoa.


!****************************************************
!**  Emulator preference in construct list         **
!**						   **
!**  MAX:  text should be less than 15 characters  **
!****************************************************
ColorMap*emuPrefMenuLabel.labelString:       Valikkorivi

ColorMap*emuPrefBlueText.labelString:        Sininen teksti

ColorMap*emuPrefGreenText.labelString:       Vihr. teksti

ColorMap*emuPrefPinkText.labelString:        V.pun. teksti

ColorMap*emuPrefRedText.labelString:         Pun. teksti

ColorMap*emuPrefTurquoiseText.labelString:   Turk. teksti

ColorMap*emuPrefWhiteText.labelString:       Valkoinen teksti

ColorMap*emuPrefYellowText.labelString:      Keltainen teksti

ColorMap*emuPrefPbBackground.labelString:    Painike

ColorMap*emuPrefStatusLine.labelString:      Tilarivi

ColorMap*groupBoxLabel.labelString:          Apuohjelma

ColorMap*entryAreaLabel.labelString:         Sy�tt�alue

ColorMap*helperFormLabel.labelString:        Button


!**************************
!**  Error dialog boxes  **
!**************************
ColorMap*noColorCellsMsgText.value: V�rien valintaohjelman aloituksessa tarvittavien v�riresurssien varaus ei onnistu. Sulje useita v�rej� k�ytt�v� sovellus ja yrit� uudelleen.

ColorMap*setColorMsgText.value:      Kaikkia emulointi-istunnon v�rej� ei ole asetettu oikein. Tallennus-vaihtoehto tallentaa kelvolliset asetukset.

ColorMap*setColorFailureCancelBtn.labelString:        Peruutus


!**********************************
!**  General Error dialog boxes  **
!**********************************
ColorMap*noAdvanced.value:            J�rjestelm� ei voi varata v�riresursseja t�ydellist� v�rien valintaa varten.

ColorMap*noAdvancedOkbutton.labelString:  OK


!*******************************************************************************
!**  quitDialog resources                                                     **
!*******************************************************************************
ColorMap*quitDialog.dialogTitle:           Lopetustoiminnon varoitus

ColorMap*quitDialog1:                      Emulointi-istunnon v�rej� on muutettu, mutta niit� ei ole otettu k�ytt��n tai tallennettu.

ColorMap*quitDialog2:                      Emulointi-istunnon v�rej� on muutettu ja ne on otettu k�ytt��n, mutta niit� ei ole tallennettu.

ColorMap*quitDialog3:                      Emulointi-istunnon v�rej� on muutettu ja ne on tallennettu, mutta niit� ei ole otettu k�ytt��n.

ColorMap*quitDialog*exitBox.labelString:   Lopetus

ColorMap*quitDialog*cancelBox.labelString: Peruutus

ColorMap*quit1Tog1:                        Lopetus ilman muutosten k�ytt��nottoa tai tallennusta.

ColorMap*quit2Tog1:                        Lopetus ilman muutosten tallennusta.

ColorMap*quitDialog*quitTog2.labelString:  Muutosten k�ytt��notto ja tallennus ennen lopetusta.

ColorMap*quit3Tog1:                        Lopetus ilman muutosten k�ytt��nottoa.

ColorMap*quitTog3.labelString:             Muutosten k�ytt��notto t�ss� istunnossa ennen lopetusta.

ColorMap*quitDialog*quitTog4.labelString:  Muutosten tallennus ennen lopetusta.


!*******************************************************************************
!**  Informational Messages                                                   **
!*******************************************************************************
ColorMap*InfoMsgColorsSaved:    V�rit on tallennettu tiedostoon:

ColorMap*tempApp:               V�rit on otettu tilap�isesti k�ytt��n emulointi-istunnossa.

ColorMap*notLoaded:             V�ritiedoston lataus ei onnistu. Tarkista, onko tiedosto viel� olemassa.

ColorMap*errorLoad:             V�ritiedoston muoto on virheellinen.


! this resource should contain one black space character; do not translate.
ColorMap*blank:  

ColorMap*chooseApply:           Ota v�rit k�ytt��n t�ss� istunnossa valitsemalla K�ytt�-painike.

ColorMap*noAccessDir:           Hakemiston k�ytt� ei onnistu.

! this cannot be made any longer during translation
ColorMap*noOpenFile:            Seuraavan v�ritiedoston avaus ei onnistu:

ColorMap*noMakeDir:             Hakemiston luonti ei onnistu.

ColorMap*noDeterminePath:       Tiedoston saantipolun m��ritys ei onnistu.

