!
!
!
! This file contains the resource text strings for the IBM Network Station 3270, 5250, and
! VTxxx emulators, in particular the miscellaneous preferences program.

! This resource file will be stored on all servers in locale specific ASCII (819 for Latin1
! languages). This resource file is stored on the booting server as N3_5MscP.

! Resource file format:
! 1. Standard ASCII text file.
! 2. Comment lines are preceded by a "!" character in column 1 (these lines not translated).
! 3. This resource file contains text strings for the 3270 and 5250 emulators.  Much of
!    the resource text is common to all emulators. Some is specific to one emulator.
!    Miscpref = Resources used by both 3270 and 5250 emulators.
!    Miscpr32 = Resources used by the 3270 emulator.
!    Miscpr52 = Resources used by the 5250 emulator.
!    MiscprVT = Resources used by the VT emulator.
! 4. Resource format: 
!    - A resource line starts with Miscpref*, Miscpr32*, Miscpr52*,or MiscprVT*, followed by 
!      a resource name and a ":".  Do NOT make any changes to the text to the left
!      of the first ":"
!    - Text to the right of the first ":" on a line is translated.

!*******************************************************************
!*******   Miscellaneous Preferences Resources              ***********
!*******************************************************************
Miscpr52*title:     Prefer�ncias Diversas de 5250

Miscpr32*title:     Prefer�ncias Diversas de 3270

MiscprVT*title:     Prefer�ncias Diversas do Emulador VT

Miscpref*iconName:  Prefer�ncias Diversas

Miscpref*okLabelString: OK

Miscpref*cancelLabelString: Cancelar


!***********************************************
!**     Action Bar Area Resources             **
!***********************************************
Miscpref*saveButton.labelString: Guardar

Miscpref*exitButton.labelString: Sair

Miscpref*setDefaultsButton.labelString: Definir Valores Assumidos

Miscpref*helpButton.labelString: Ajuda


!*********************************************************
!**     Preference Buttons Area Resources               **
!**							**
!*********************************************************
Miscpref*preferencesListInstructions.labelString: Seleccione uma prefer�ncia

Miscpref*CursorStyleItem:                         Estilo do Cursor

Miscpr52*EnterKeyLocationItem:                    Tecla Enter/Sa�da de Campo

Miscpr32*EnterKeyLocationItem:                    Tecla Enter
	
Miscpr52*AutomaticHelpItem:                       Ajuda Autom�tica

Miscpr52*BlueUnderscoreItem:                      Sublinhado Azul

Miscpref*RowColumnIndicatorItem:                  Indicador Linha/Coluna

Miscpref*DestructiveBackspaceItem:                Retrocesso Destrutivo

Miscpref*RuleLineEnableItem:                      Activar Linha de R�gua

Miscpref*RuleLineStyleItem:                       Estilo da Linha da R�gua

Miscpref*PasteStartLocationItem:                  Localiza��o de In�cio de Colar

Miscpref*CursorBlinkItem:                         Cursor a Piscar

Miscpr52*LargeScreenBehaviorItem:                 Comportamento com Ecr� Grande

Miscpref*PrintKeyLocationItem:                    Tecla de Impress�o

Miscpr32*BracketItem:                             Par�nteses recto

Miscpr32*AutomaticReconnectItem:                  Reposi��o Autom�tica da Liga��o

Miscpref*HotspotsItem:                            Pontos Activos

Miscpref*TypeAheadItem:                           Buffer de Teclado

Miscpr52*ErrorResetKeys:                          Teclas de Reposi��o de Erro

Miscpref*HotspotHighlight:                        Evid�ncia de Hotspot

Miscpref*DefaultCopyType:                         Tipo de C�pia Assumida

Miscpref*InsertMode:                              Modo de Inser��o

Miscpref*InputOnlyCursorMovement:                 Cursor Apenas de Entrada

Miscpref*AudioAlarm:                              Alarme �udio

MiscprVT*CursorBlinkItem:           Cursor a Piscar

MiscprVT*ApplicationCursor:         Modo de Cursor da Aplica��o

MiscprVT*ApplicationKeypad:         Modo de Teclado Auxiliar da Aplica��o

MiscprVT*AutoLineFeed:              Mudan�a de Linha Autom�tica

MiscprVT*AutoResize:                Redimensionamento Autom�tico

MiscprVT*AutoWrapAround:            Transposi��o Autom�tica

MiscprVT*ClearScreenWithBlanks:     Limpar Ecr� com Brancos

MiscprVT*ExitOnDisconnect:          Sair ao Desligar

MiscprVT*JumpScroll:                Salto de P�gina

MiscprVT*MarginBell:                Aviso de Margem

MiscprVT*MarginBellColumn:          Coluna do Aviso de Margem

MiscprVT*ReverseWrapAround:         Transposi��o Inversa

MiscprVT*VisibleStatusLine:         Linha de Estado Vis�vel

MiscprVT*VisualBell:                Aviso Visual
	
!*************************************************************
!**     Preference Text Area Resources (Explanation Box)    **
!*************************************************************
Miscpref*CursorStyleText.value: Os poss�veis estilos de cursor incluem cursores sublinhados e em forma de bloco.  O estilo do cursor pode ser sempre definido como sublinhado, sempre como bloco ou como uma combina��o de sublinhado e bloco dependendo ou n�o do modo de inser��o estar activo.

MiscprVT*CursorStyleText.value: O estilo do cursor pode ser definido sempre para sublinhado ou para bloco.

MiscprVT*CursorBlinkText.value: A intermit�ncia do cursor pode ser activada ou desactivada.  Se for activada, o cursor ir� apenas piscar quando a janela estiver activa.

MiscprVT*ApplicationCursorText.value: As teclas de cursor podem estar inicialmente em modo de aplica��o ou em modo de cursor. Quando as teclas das setas s�o usadas em modo de aplica��o, s�o geradas sequ�ncias de altera��o de controlo ANSI em vez de se usarem movimentos padr�o de cursor.

MiscprVT*ApplicationKeypadText.value: As teclas de cursor podem estar inicialmente em modo de aplica��o ou em modo num�rico. Quando o teclado num�rico � usado em modo de aplica��o, s�o geradas fun��es de controlo em vez de caracteres num�ricos.

Miscpr52*EnterKeyLocationText.value: As teclas das fun��es Enter, nova linha e sa�da de campo podem ser alteradas a partir das suas defini��es iniciais enunciadas abaixo. S�o fornecidas v�rias combina��es dessas fun��es na tecla Enter e de Ctrl direito.

Miscpr32*EnterKeyLocationText.value: As teclas das fun��es Enter e nova linha podem ser alteradas a partir das suas defini��es iniciais enunciadas abaixo. S�o fornecidas v�rias combina��es dessas fun��es na tecla Enter e de Ctrl direito.

Miscpr52*AutomaticHelpText.value: A tecla de Ajuda pode ser pressionada automaticamente como consequ�ncia de um erro de teclado por parte do operador.  Tipicamente uma tecla de Ajuda autom�tica apresenta uma linha de texto de ajuda de primeiro n�vel a explicar esse erro de operador. Ao desactivar a tecla de Ajuda autom�tica ser� apresentado o erro de operador de quatro d�gitos. Nesse caso, o utilizador pode ainda pressionar a tecla de Ajuda manualmente para obter texto de ajuda de primeiro n�vel.

Miscpr52*BlueUnderscoreText.value: A cor assumida do sublinhado pode ser alterada para azul.

Miscpref*RowColumnIndicatorText.value: O indicador de linha/coluna apresenta a linha e a coluna correspondentes � posi��o do cursor, e est� localizado no lado direito da linha de estado. O indicador de linha/coluna pode ser apresentado ou ocultado. Note que, no caso do cursor n�o estar apresentado, o indicador de linha/coluna tamb�m n�o � apresentado, independentemente desta defini��o.

Miscpref*DestructiveBackspaceText.value: A fun��o de retrocesso pode ser destrutiva ou n�o-destrutiva. No caso de ser destrutiva, a fun��o de retrocesso apaga os dados � medida que o cursor se movimenta para tr�s ao longo dos campos de entrada de dados. No caso de ser n�o-destrutiva, a fun��o de retrocesso n�o apaga os dados � medida que o cursor se movimenta para tr�s ao longo dos campos de entrada de dados.

Miscpref*RuleLineEnableText.value: A linha de r�gua mostra a localiza��o, ou localiza��o anterior, do cursor. Pode ser completamente desactivada, activada permanentemente, ou controlada pela tecla de linha de r�gua. Quando controlada pela linha de r�gua, pode permanecer estacion�ria ou pode seguir o cursor.

Miscpref*RuleLineStyleText.value: Quando a linha de r�gua � apresentada, pode tomar uma de tr�s formas: a de uma linha horizontal, a de uma linha vertical, ou de duas linhas sendo uma horizontal e outra vertical.

Miscpref*PasteStartLocationText.value: A opera��o de colar associada � press�o do segundo bot�o do rato simultaneamente com a tecla Shift pode ser definida por forma a ocorrer na posi��o do cursor ou na posi��o do ponteiro do rato. Note que esta prefer�ncia n�o afecta a opera��o de colagem originada a partir do menu pendente. Este �ltimo tipo de colagem efectuada a partir do menu pendente � processada na posi��o do cursor.

Miscpr52*CursorBlinkText.value: A intermit�ncia do cursor pode ser activada ou desactivada.  Quando activada a intermit�ncia do cursor est� sob controlo aplicacional.  Quando desactivada, o cursor nunca pisca.

Miscpr32*CursorBlinkText.value: A intermit�ncia do cursor pode ser activada ou desactivada.  Quando activada, o cursor pisca sempre. Quando desactivada, o cursor nunca pisca.

Miscpr52*LargeScreenBehaviorText.value: O emulador 5250 pode ser configurado para mover automaticamente a janela e/ou reduzir o tamanho de fonte em caso de necessidade, quando � introduzido um ecr� de tamanho grande. Isto � feito quando um novo tamanho de janela n�o cabe no ecr� na posi��o actual e/ou com o tamanho de fonte corrente.

Miscpr52*PrintKeyLocationText.value: A n�o ser que seja substitu�da por redefini��o do teclado, a defini��o da tecla de Impress�o (Print) corresponde � fun��o de impress�o de sistema, enquanto que a tecla de Impress�o (Print) conjuntamente com Shift proporciona a fun��o de impress�o local do ecr�. Alguns utilizadores podem preferir a tecla de Impress�o (Print) para proporcionar a fun��o de impress�o local do ecr�, e a tecla de Impress�o (Print) conjuntamente com Shift para proporcionar a fun��o de impress�o de sistema.

Miscpr32*PrintKeyLocationText.value: A n�o ser que seja substitu�da por redefini��o do teclado, a defini��o da tecla de Impress�o (Print) � a fun��o pa1, enquanto que a tecla de Impress�o (Print) conjuntamente com Shift corresponde � fun��o de impress�o local do ecr�.  Alguns utilizadores podem preferir a tecla de Impress�o (Print) para proporcionar a fun��o de impress�o local do ecr�, e a tecla de Impress�o (Print) conjuntamente com Shift para proporcionar a fun��o pa1.

Miscpr32*BracketText.value: Ao activar a op��o de par�nteses recto passa a ser permitida a convers�o correcta dos caracteres de par�nteses recto.

Miscpr32*AutomaticReconnectText.value: A janela do emulador 3270 pode ser removida ou deixada no ecr� quando um utilizador termina a sess�o. Quando deixada no ecr� o emulador tenta automaticamente voltar a ligar ao sistema central.

Miscpref*HotspotsText.value: Os pontos activos s�o posi��es de ecr� que executam determinadas ac��es quando � feito clique com o ponteiro do rato sobre eles. Por exemplo, com o ponteiro do rato sobre 'PF1' ou 'F1', e com os pontos activos ligados, um clique � processado como se a tecla F1 tivesse sido pressionada. Os pontos activos podem estar desligados, serem ligados ao clique do bot�o principal do rato, ou serem ligados ao duplo clique do bot�o principal do rato.

Miscpref*TypeAheadText.value: O buffer de teclado permite armazenar os batimentos de teclas que s�o efectuados enquanto o teclado est� bloqueado (inibi��o de entrada). Estas teclas s�o processadas mais tarde quando o teclado desbloqueia. A activa��o do buffer de teclado for�a a que esteja sempre activo. A desactiva��o do buffer de teclado for�a a que esteja sempre inactivo.

Miscpr52*ErrorResetKeysText.value: As teclas do emulador 5250 para recuperar erros podem ser configuradas abaixo. A tecla reset e um clique com o bot�o esquerdo do rato sobre a linha do erro permite sempre a reposi��o ap�s o erro. Al�m disso, os utilizadores podem tamb�m optar por terem teclas de movimento de cursor (tabula��o, novalinha, setas, retrocesso) e outros cliques de rato para a reposi��o ap�s erros.

Miscpref*HotspotHighlightText.value: O emulador pode ser configurado para real�ar os hotspots. Se os hotspots estiverem activos e se a evid�ncia de hotspots estiver tamb�m activa, quaisquer hotspots do ecr� s�o evidenciados.

Miscpref*DefaultCopyTypeText.value: O tipo de c�pia assumido corresponde ao tipo de �rea de c�pia associado � opera��o de pressionar e arrastar do bot�o principal do rato. � tamb�m o tipo de c�pia associado � marca��o de �rea de c�pia do teclado.

Miscpr52*InsertModeText.value: O modo de inser��o assumido � desactivado; para al�m da tecla de inser��o que o pode alterar, � por vezes reposto automaticamente por aplica��es e pela tecla de reposi��o. Esta reposi��o autom�tica do modo de inser��o pode ser desactivada, o que poder� ser conseguido com o modo de inser��o assumido como activo ou desactivado. 
Miscpr32*InsertModeText.value: O modo de inser��o assumido � desactivado; para al�m da tecla de inser��o que o pode alterar, � reposto automaticamente por interac��o com aplica��es, tal como com a tecla enter, e por vezes pela tecla de reposi��o. Esta reposi��o autom�tica do modo de inser��o pode ser desactivada, o que poder� ser conseguido com o modo de inser��o assumido como activo ou desactivado.

Miscpref*InputOnlyCursorMovementText.value: As teclas de seta podem ser configuradas por forma a mover o cursor apenas para posi��es com capacidade de entrada de dados. Se tal for a op��o, ent�o as teclas de seta ir�o sempre mover o cursor para posi��es com capacidade de entrada de dados. Se tais posi��es n�o existirem, o cursor desloca-se para a primeira posi��o no ecr�.

Miscpref*AudioAlarmText.value: O alarme �udio pode ser activado ou desactivado. Quando est� desactivado, o alarme �udio nunca � emitido.

MiscprVT*AutoLineFeedText.value: A mudan�a de linha autom�tica pode ser activada ou desactivada. Quando est� activada, o Emulador VT executa um retorno � margem e uma mudan�a de linha quando um car�cter de mudan�a de linha, tabula��o vertical ou avan�o de p�gina � enviado pela aplica��o.  Para al�m disso, quaisquer teclas que gerem uma nova linha (tal como a tecla enter) enviar�o um retorno � margem e uma mudan�a de linha para a aplica��o.  Quando est� desactivada, s� ser� executada a fun��o salto de linha quando � recebida uma mudan�a de linha, tabula��o vertical ou avan�o de p�gina.  Quando est� desactivada e quando a tecla enter � premida, s� � enviada uma mudan�a de linha para a aplica��o.

MiscprVT*AutoResizeText.value: O redimensionamento autom�tico pode estar activado ou desactivado.

MiscprVT*AutoWrapAroundText.value: O modo de translinea��o autom�tico pode ser activado ou desactivado. Quando este modo � activado, o car�cter que for introduzido ap�s o cursor atingir o limite do ecr�, ir� aparecer automaticamente na linha seguinte. Se a translinea��o autom�tica for desactivada, ent�o um car�cter que seja introduzido ap�s o cursor ter atingido o limite do ecr�, ir� substituir o car�cter no fim da linha.

MiscprVT*ClearScreenWithBlanksText.value: O ecr� pode ser opcionalmente limpo com espa�os em branco.

MiscprVT*ExitOnDisconnectText.value: O terminal pode ser configurado para terminar ao desligar-se do sistema central.

MiscprVT*JumpScrollText.value: O terminal pode ser configurado para avan�ar rapidamente. Com o avan�o r�pido, o ecr� pode ser movimentado mais do que uma linha de cada vez. Esta ac��o fornece actualiza��es de ecr� mais r�pidas quando s�o enviadas para o ecr� v�rias linhas de texto. O n�mero m�ximo de linhas que podem avan�ar rapidamente est� limitado pelo n�mero de linhas da janela do emulador VT.

MiscprVT*MarginBellText.value: O terminal pode ser configurado para emitir um aviso sonoro quando o utilizador escrever na margem direita.

MiscprVT*MarginBellColumnText.value: Especifique a coluna para o sinal sonoro de margem.

MiscprVT*ReverseWrapAroundText.value: A translinea��o inversa pode ser activada ou desactivada. A translinea��o inversa permite que o cursor passe da primeira posi��o da linha para a �ltima coluna da linha anterior, permitindo ao utilizador retroceder � linha anterior. A translinea��o autom�tica tamb�m de estar activada para que a translinea��o inversa possa trabalhar.

MiscprVT*VisibleStatusLineText.value: A linha de estado pode estar vis�vel ou invis�vel. Se a linha de estado estiver vis�vel, � visualizada uma 25� linha no fundo da janela do emulador VT. Esta linha pode ser usada pelas aplica��es para mostrar informa��o de estado. Quando n�o estiver a ser usada por uma aplica��o, esta linha apresenta um indicador de linha/coluna.

MiscprVT*VisualBellText.value: Um aviso visual (uma intermit�ncia de ecr�) pode ser substitu�do por um aviso aud�vel.

!*************************************************************
!**     Preference Text Area Resources (Options area)       **
!**							                                                  **
!*************************************************************

Miscpref*PreferenceTextFormForCursorStyle*ToggleButton1.labelString: Cursor sublinhado

Miscpref*PreferenceTextFormForCursorStyle*ToggleButton2.labelString: Cursor de bloco

Miscpref*PreferenceTextFormForCursorStyle*ToggleButton3.labelString: Cursor de inser��o em bloco, cursor de substitui��o sublinhado

Miscpref*PreferenceTextFormForCursorStyle*ToggleButton4.labelString: Cursor de inser��o sublinhado, cursor de substitui��o em bloco

Miscpref*PreferenceTextFormForEnterKeyLocation*ToggleButton1.labelString: Utilizar as defini��es de teclas assumidas para Enter e Right Ctrl

Miscpref*PreferenceTextFormForEnterKeyLocation*ToggleButton2.labelString: Tecla Enter = enter, Ctrl Direito = nova linha

Miscpref*PreferenceTextFormForEnterKeyLocation*ToggleButton3.labelString: Tecla Enter = nova linha, Ctrl Direito = enter

Miscpr52*PreferenceTextFormForEnterKeyLocation*ToggleButton4.labelString: Tecla Enter = enter, Ctrl Direito = sa�da de campo/nova linha

Miscpr52*PreferenceTextFormForEnterKeyLocation*ToggleButton5.labelString: Tecla Enter = sa�da de campo/nova linha, Ctrl Direito = enter

Miscpr52*PreferenceTextFormForAutomaticHelp*ToggleButton1.labelString: Activar a tecla de ajuda autom�tica para erros do operador

Miscpr52*PreferenceTextFormForAutomaticHelp*ToggleButton2.labelString: Desactivar a tecla de ajuda autom�tica para erros do operador

Miscpr52*PreferenceTextFormForBlueUnderscore*ToggleButton1.labelString: Activar sublinhado azul

Miscpr52*PreferenceTextFormForBlueUnderscore*ToggleButton2.labelString: Desactivar sublinhado azul

Miscpref*PreferenceTextFormForRowColumnIndicator*ToggleButton1.labelString: Mostrar indicador de linha e coluna

Miscpref*PreferenceTextFormForRowColumnIndicator*ToggleButton2.labelString: N�o mostrar indicador de linha e coluna

Miscpref*PreferenceTextFormForDestructiveBackspace*ToggleButton1.labelString: Activar retrocesso destrutivo

Miscpref*PreferenceTextFormForDestructiveBackspace*ToggleButton2.labelString: Desactivar retrocesso destrutivo

Miscpref*PreferenceTextFormForRuleLineEnable*ToggleButton1.labelString: Desactivar linha de r�gua

Miscpref*PreferenceTextFormForRuleLineEnable*ToggleButton2.labelString: Mostrar sempre a linha de r�gua na posi��o do cursor

Miscpref*PreferenceTextFormForRuleLineEnable*ToggleButton3.labelString: Activar a tecla de linha de r�gua (sem seguir o cursor)

Miscpref*PreferenceTextFormForRuleLineEnable*ToggleButton4.labelString: Activar a tecla de linha de r�gua (seguindo o cursor)

Miscpref*PreferenceTextFormForRuleLineStyle*ToggleButton1.labelString: Linhas de r�gua horizontal e vertical simult�neas

Miscpref*PreferenceTextFormForRuleLineStyle*ToggleButton2.labelString: Apenas linha de r�gua horizontal

Miscpref*PreferenceTextFormForRuleLineStyle*ToggleButton3.labelString: Apenas linha de r�gua vertical

Miscpref*PreferenceTextFormForPasteStartLocation*ToggleButton1.labelString: Colar na posi��o do cursor

Miscpref*PreferenceTextFormForPasteStartLocation*ToggleButton2.labelString: Colar na posi��o do rato

Miscpref*PreferenceTextFormForCursorBlink*ToggleButton1.labelString: Desactivar intermit�ncia do cursor

Miscpref*PreferenceTextFormForCursorBlink*ToggleButton2.labelString: Activar intermit�ncia do cursor

Miscpr52*PreferenceTextFormForLargeScreenBehavior*ToggleButton1.labelString: N�o mover a janela nem reduzir o tamanho da fonte

Miscpr52*PreferenceTextFormForLargeScreenBehavior*ToggleButton2.labelString: Mover a janela e, se necess�rio, reduzir o tamanho da fonte

Miscpr52*PreferenceTextFormForLargeScreenBehavior*ToggleButton3.labelString: Reduzir apenas o tamanho da fonte

Miscpr52*PreferenceTextFormForLargeScreenBehavior*ToggleButton4.labelString:  Usar/Memorizar Fonte de Ecr� Grande

Miscpr52*PreferenceTextFormForLargeScreenBehavior*ToggleButton5.labelString: Reduzir o tamanho da fonte e, se necess�rio, mover a janela

Miscpr52*PreferenceTextFormForPrintKeyLocation*ToggleButton1.labelString: Usar as defini��es assumidas da tecla de Impress�o (Print)

Miscpr52*PreferenceTextFormForPrintKeyLocation*ToggleButton2.labelString: Print = impress�o de sistema, Print com Shift = impress�o local

Miscpr52*PreferenceTextFormForPrintKeyLocation*ToggleButton3.labelString: Print = impress�o local, Print com Shift = impress�o de sistema

Miscpr32*PreferenceTextFormForPrintKeyLocation*ToggleButton1.labelString: Usar as defini��es assumidas da tecla de Impress�o (Print)

Miscpr32*PreferenceTextFormForPrintKeyLocation*ToggleButton2.labelString: Print = pa1, Print com Shift = impress�o local

Miscpr32*PreferenceTextFormForPrintKeyLocation*ToggleButton3.labelString: Print = impress�o local, Print com Shift = pa1

Miscpr32*PreferenceTextFormForBracket*ToggleButton1.labelString: Desactivar convers�o de par�nteses rectos

Miscpr32*PreferenceTextFormForBracket*ToggleButton2.labelString: Activar convers�o de par�nteses rectos

Miscpr32*PreferenceTextFormForAutomaticReconnect*ToggleButton1.labelString: A janela � removida depois de terminar sess�o

Miscpr32*PreferenceTextFormForAutomaticReconnect*ToggleButton2.labelString: A janela permanece depois de terminar sess�o

Miscpref*PreferenceTextFormForHotspots*ToggleButton1.labelString: Desactivar hotspots

Miscpref*PreferenceTextFormForHotspots*ToggleButton2.labelString: Activar hotspots com um clique do bot�o principal do rato

Miscpref*PreferenceTextFormForHotspots*ToggleButton3.labelString: Activar hotspots com um duplo clique do bot�o principal do rato

Miscpref*PreferenceTextFormForTypeAhead*ToggleButton1.labelString: Desactivar buffer do teclado

Miscpref*PreferenceTextFormForTypeAhead*ToggleButton2.labelString: Activar buffer do teclado

Miscpr52*PreferenceTextFormForTypeAhead*ToggleButton3.labelString: Usar a defini��o de AS/400

Miscpr52*PreferenceTextFormForErrorResetKeys*ToggleButton1.labelString: Tecla de reset ou bot�o esquerdo do rato apenas na linha de erro.

Miscpr52*PreferenceTextFormForErrorResetKeys*ToggleButton2.labelString: Adicionalmente, as teclas de movimento do cursor e todos os bot�es do rato

Miscpr52*PreferenceTextFormForErrorResetKeys*ToggleButton3.labelString: A maior parte das teclas e todos os bot�es do rato

Miscpref*PreferenceTextFormForHotspotHighlight*ToggleButton1.labelString: Desactivar realce dos hotspots

Miscpref*PreferenceTextFormForHotspotHighlight*ToggleButton2.labelString: Activar realce dos hotspots

Miscpref*PreferenceTextFormForDefaultCopyType*ToggleButton1.labelString: C�pia linear

Miscpref*PreferenceTextFormForDefaultCopyType*ToggleButton2.labelString: C�pia em bloco

Miscpref*PreferenceTextFormForInsertMode*ToggleButton1.labelString: Modo de inser��o assumido desactivado com reposi��o autom�tica activa

Miscpref*PreferenceTextFormForInsertMode*ToggleButton2.labelString: Modo de inser��o assumido desactivado com reposi��o autom�tica desactivada

Miscpref*PreferenceTextFormForInsertMode*ToggleButton3.labelString: Modo de inser��o assumido activado com reposi��o autom�tica desactivada

Miscpref*PreferenceTextFormForInputOnlyCursorMovement*ToggleButton1.labelString:  Desactivar movimento do cursor apenas para entrada de dados

Miscpref*PreferenceTextFormForInputOnlyCursorMovement*ToggleButton2.labelString: Activar movimento do cursor apenas para entrada de dados

Miscpref*PreferenceTextFormForAudioAlarm*ToggleButton1.labelString: Desactivar alarme �udio

Miscpref*PreferenceTextFormForAudioAlarm*ToggleButton2.labelString: Activar alarme �udio

MiscprVT*PreferenceTextFormForCursorBlink*ToggleButton1.labelString: Desactivar intermit�ncia do cursor

MiscprVT*PreferenceTextFormForCursorBlink*ToggleButton2.labelString: Activar intermit�ncia do cursor

MiscprVT*PreferenceTextFormForApplicationCursor*ToggleButton1.labelString: Desactivar modo de cursor de aplica��o

MiscprVT*PreferenceTextFormForApplicationCursor*ToggleButton2.labelString: Activar modo de cursor de aplica��o

MiscprVT*PreferenceTextFormForApplicationKeypad*ToggleButton1.labelString: Desactivar modo de teclado auxiliar da aplica��o

MiscprVT*PreferenceTextFormForApplicationKeypad*ToggleButton2.labelString: Activar modo de teclado auxiliar da aplica��o

MiscprVT*PreferenceTextFormForAutoLineFeed*ToggleButton1.labelString: Desactivar mudan�a de linha autom�tica

MiscprVT*PreferenceTextFormForAutoLineFeed*ToggleButton2.labelString: Activar mudan�a de linha autom�tica

MiscprVT*PreferenceTextFormForAutoResize*ToggleButton1.labelString: Desactivar redimensionamento autom�tico

MiscprVT*PreferenceTextFormForAutoResize*ToggleButton2.labelString: Activar redimensionamento autom�tico

MiscprVT*PreferenceTextFormForAutoWrapAround*ToggleButton1.labelString: Desactivar translinea��o autom�tica

MiscprVT*PreferenceTextFormForAutoWrapAround*ToggleButton2.labelString: Activar translinea��o autom�tica

MiscprVT*PreferenceTextFormForClearScreenWithBlanks*ToggleButton1.labelString: Desactivar limpeza de ecr� com espa�os em branco

MiscprVT*PreferenceTextFormForClearScreenWithBlanks*ToggleButton2.labelString: Activar limpeza de ecr� com espa�os em branco

MiscprVT*PreferenceTextFormForExitOnDisconnect*ToggleButton1.labelString: Desactivar sair ao desligar

MiscprVT*PreferenceTextFormForExitOnDisconnect*ToggleButton2.labelString: Activar sair ao desligar

MiscprVT*PreferenceTextFormForJumpScroll*ToggleButton1.labelString: Desactivar avan�o r�pido

MiscprVT*PreferenceTextFormForJumpScroll*ToggleButton2.labelString: Activar avan�o r�pido

MiscprVT*PreferenceTextFormForMarginBell*ToggleButton1.labelString: Desactivar aviso de margem

MiscprVT*PreferenceTextFormForMarginBell*ToggleButton2.labelString: Activar aviso de margem

MiscprVT*PreferenceTextFormForMarginBellColumn*textFieldLabel.labelString: Colunas a partir do fim da linha

MiscprVT*PreferenceTextFormForReverseWrapAround*ToggleButton1.labelString: Desactivar translinea��o inversa

MiscprVT*PreferenceTextFormForReverseWrapAround*ToggleButton2.labelString: Activar translinea��o inversa

MiscprVT*PreferenceTextFormForVisibleStatusLine*ToggleButton1.labelString: Desactivar linha de estado

MiscprVT*PreferenceTextFormForVisibleStatusLine*ToggleButton2.labelString: Activar linha de estado

MiscprVT*PreferenceTextFormForVisualBell*ToggleButton1.labelString: Desactivar o aviso visual

MiscprVT*PreferenceTextFormForVisualBell*ToggleButton2.labelString: Activar o aviso visual



!**************************************
!**   Messages at bottom of window   **
!**************************************

Miscpr52*MiscprefMsgHelpRequested:     Ajuda pedida na sess�o de 5250.

Miscpr32*MiscprefMsgHelpRequested:     Ajuda pedida na sess�o de 3270.

MiscprVT*MiscprefMsgHelpRequested:     Ajuda pedida na sess�o de Emulador VT.

Miscpref*MiscprefMsgPreferenceApplied: Prefer�ncia temporariamente aplicada.

Miscpref*MiscprefMsgPreferencesSaved:  Prefer�ncias guardadas.

Miscpref*MiscprefMsgDefaultsApplied:   Prefer�ncias assumidas temporariamente aplicadas.

Miscpref*MiscprefMsgDefaultsNotSet:    Prefer�ncias assumidas n�o definidas.

Miscpref*MiscprefMsgNoAccess:          Imposs�vel aceder ao direct�rio.


Miscpref*MiscprefMsgNoMake:            Imposs�vel criar direct�rio.

Miscpref*MiscprefMsgFileOpenError:     Erro ao guardar:  Imposs�vel abrir o ficheiro.

Miscpref*MiscprefMsgFileWriteError:    Erro ao guardar:  Imposs�vel escrever no ficheiro.



!********************************
!**  Dialog Box Resources      **
!********************************
Miscpref*exitDialog.dialogTitle:              Aviso de Sa�da

Miscpref*exitDialog.okLabelString:            Guardar

Miscpref*exitDialog.cancelLabelString:        N�o guardar

! this cannot be much longer
Miscpref*exitDialog.messageString:            Existem altera��es �s prefer�ncias diversas n�o guardadas.

Miscpref*setDefaultsDialog.dialogTitle:       Aviso de Defini��o de Valores Assumidos

Miscpref*setDefaultsDialog.okLabelString:     Sim

Miscpref*setDefaultsDialog.cancelLabelString: N�o

! this cannot be much longer
Miscpref*setDefaultsDialog.messageString:     Pretende realmente definir todas as prefer�ncias para os valores assumidos?
/*******************************************************************************
**  saveDialog resource
*******************************************************************************/
MiscprVT*saveDialog.dialogTitle: Op��es de Guardar

MiscprVT*saveDialog*saveDialogTitle.labelString: Seleccione uma op��o de guardar.

MiscprVT*saveDialog*saveChangedOnly.labelString: Guardar apenas prefer�ncias alteradas

MiscprVT*saveDialog*saveAll.labelString: Guardar todas as prefer�ncias

MiscprVT*saveDialog*saveButton.labelString: Guardar

MiscprVT*saveDialog*cancelButton.labelString: Cancelar

