! ENGLISH_SOURCE_VERSION= 1.4.1.2 
!
!
!
! This file contains the resource text strings for the IBM Network Station 3270 and 5250
! emulators, in particular the color mapping program.

! This resource file will be stored on all servers in locale specific ASCII (819 for Latin1
! languages). 

! Resource file format:
! 1. Standard ASCII text file.
! 2. Comment lines are preceded by a "!" character in column 1 (these lines not translated).
! 3. Resource format: 
!    - A resource line starts with ColorMap* followed by a resource name
!      and a ":".  Do NOT make any changes to the text to the left of the first ":"
!    - Text to the right of the first ":" on a line is translated.

!******************************************************************************
!************************ BEGIN DO NOT TRANSLATE ******************************
!******************************************************************************
!*   The first section of resources are are locale specific but               *
!*   DO NOT REQUIRE TRANSLATION.                                              *
!*                                                                            *
!* This source file enables the resource section for Latin1 and Latin2.       *
!*                                                                            *
!* For ru_RU or Latin5 locales and DBCS locales                               *
!*      1. Place a '!' as the first character in each line in the Latin1      *
!*      and Latin2 section                                                    *
!*      2. Remove the '!' character for each resource line in the apropriate  *
!*      section that matches your locale.                                     *
!*                                                                            *
!* For example, ru_RU should place ! in front of all lines in the LATIN1      *
!* section remove the ! from all lines in the LATIN5 section.                 *      
!******************************************************************************

!**************************************************************************
!****************   BEGIN LATIN1 and LATIN2                ****************
!**************************************************************************
ColorMap*extraLargeFontList: -*-*-medium-r-normal-*-*-*-*-*-m-110-*-*
ColorMap*largeFontList:      -*-*-medium-r-normal-*-*-*-*-*-m-90-*-*
ColorMap*mediumFontList:     -*-*-medium-r-normal-*-*-*-*-*-m-70-*-*
ColorMap*smallFontList:      -*-*-medium-r-normal-*-*-*-*-*-m-60-*-*
!**************************************************************************
!****************   END LATIN1 and LATIN2                  ****************
!**************************************************************************

!**************************************************************************
!****************   BEGIN LATIN5                           ****************
!**************************************************************************
!ColorMap*extraLargeFontList: -*-*-medium-r-normal-*-*-*-*-*-c-100-*-*
!ColorMap*largeFontList:      -*-*-medium-r-normal-*-*-*-*-*-c-90-*-*
!ColorMap*mediumFontList:     -*-*-medium-r-normal-*-*-*-*-*-c-70-*-*
!ColorMap*smallFontList:      -*-*-medium-r-normal-*-*-*-*-*-c-60-*-*
!**************************************************************************
!****************   END   LATIN5                           ****************
!**************************************************************************

!**************************************************************************
!****************   BEGIN DBCS (ja_JP ko_KR zh_CN)   ****************
!**************************************************************************
!ColorMap*extraLargeFontList: -*-*-medium-r-normal--24-*-*-*-c-*
!ColorMap*largeFontList:      -*-*-medium-r-normal--16-*-*-*-c-*
!ColorMap*mediumFontList:     -*-*-medium-r-normal--16-*-*-*-c-*
!ColorMap*smallFontList:      -*-*-medium-r-normal--16-*-*-*-c-*
!**************************************************************************
!****************   END DBCS (ja_JP ko_KR zh_CN)     ****************
!**************************************************************************
!**************************************************************************
!****************   BEGIN DBCS (zh_TW)   ****************
!**************************************************************************
!ColorMap*extraLargeFontList: -*-*-medium-r-normal--24-*-*-*-c-*,-arphic-ar pl mingti2l big5-medium-r-normal--24-240-0-0-c-*
!ColorMap*largeFontList:      -*-*-medium-r-normal--16-*-*-*-c-*,-arphic-ar pl mingti2l big5-medium-r-normal--16-160-0-0-c-*
!ColorMap*mediumFontList:     -*-*-medium-r-normal--16-*-*-*-c-*,-arphic-ar pl mingti2l big5-medium-r-normal--16-160-0-0-c-*
!ColorMap*smallFontList:      -*-*-medium-r-normal--16-*-*-*-c-*,-arphic-ar pl mingti2l big5-medium-r-normal--16-160-0-0-c-*
!**************************************************************************
!****************   END DBCS (zh_TW)     ****************
!**************************************************************************

!**************************************************************************
!****************   END DO NOT TRANSLATE                   ****************
!**************************************************************************

ColorMap*title: 			D�finition de couleurs

! icons
ColorMap*iname3270:                     D�finition de couleurs 3270
ColorMap*iname5250:                     D�finition de couleurs 5250


ColorMap*corrupt:               Le fichier de couleurs est alt�r�

ColorMap*lowMemMsgDialog.messageString:  M�moire insuffisante pour lancer l'afficheur d'aide.

ColorMap*okLabelString: OK

ColorMap*cancelLabelString: Annulation

!*******************************************************************************
!**     Preferences area resources (top of window)                            **
!*******************************************************************************
ColorMap*modeInstructions.labelString:  S�lectionnez la fonction de d�finition de couleurs :

ColorMap*Basic.labelString:             De base

ColorMap*Advanced.labelString:          Options avanc�es


!*************************************************************
!**   Basic and advanced action bar area resources          **
!*************************************************************
ColorMap*actionForm.saveButton.labelString:  	Sauvegarde

ColorMap*actionForm.deleteButton.labelString:   Suppression

ColorMap*actionForm.applyButton.labelString:    Appliquer � la session

ColorMap*actionForm.quitButton.labelString:     Sortie

ColorMap*actionForm.helpButton.labelString:     Aide


!************************************************
!**  Basic and advanced color savebox          **
!************************************************
ColorMap*saveBoxLabel.labelString:         Choisissez une option de sauvegarde :

ColorMap*saveBoxToggle1.labelString:       Sauvegarder par d�faut pour toutes les sessions

ColorMap*saveBoxToggle2.labelString:       Sauvegarder pour cette session uniquement

ColorMap*saveBoxSaveBtn.labelString:  	   Sauvegarde

ColorMap*saveBoxCancelBtn.labelString:     Annulation

ColorMap*advancedSaveBoxLabel.labelString: Entrez un nom de fichier de couleurs �tendues :

! Characters in file names must be 819 (ISO8859-1)
ColorMap*invalidFileName.value:            Le caract�re que vous avez tap� n'est pas admis dans un nom de fichier.

ColorMap*invalidFileNameOkbutton.labelString:  OK

!****************************************
!**  Overwrite existing file dialog    **
!****************************************
ColorMap*fileWarningDialog.dialogTitle:        Avertissement

ColorMap*fileWarningDialog.messageString:      Ce fichier existe d�j�. Voulez-vous le remplacer ?

ColorMap*fileWarningDialog.okLabelString:      Oui

ColorMap*fileWarningDialog.cancelLabelString:  Non



!***************************************
!**  Basic color work area            **
!***************************************
ColorMap*basicListLabel.labelString:        Choisissez une palette de couleurs

! The following two resource lines are a late addition to the color mapping resource file (March 12, 1998).  These indicate color map files that came from a system level or user created color map files.
ColorMap*systemLabel:                       Syst�me :

ColorMap*userLabel:                         Utilisateur :

ColorMap*ibmBlack:                          IBM : Arri�re-plan noir

ColorMap*ibmDarkGrey:                       IBM : Arri�re-plan gris fonc�

ColorMap*ibmLightGrey:                      IBM : Arri�re-plan gris clair

ColorMap*ibmLight:                          IBM : Arri�re-plan clair

ColorMap*ibmLightBlue:                      IBM : Arri�re-plan bleu clair



!******************************************
!**  Advanced color work area            **
!******************************************
ColorMap*advancedListLabel.labelString:     Choisissez l'�l�ment � modifier :

! The following 22 *adv type resources are limited in size; they should not exceed 28 characters.
ColorMap*advMenuBarBack:                    Arri�re-plan barre de menus

ColorMap*advMenuBarFore:                    Premier plan barre de menus

ColorMap*advMainSess:                       Arri�re-plan session princ.

ColorMap*advPushbut:                        Arri�re-plan btn de fonction

ColorMap*advBlue:                           Texte bleu

ColorMap*advGreen:                          Texte vert

ColorMap*advPink:                           Texte rose

ColorMap*advRed:                            Texte rouge

ColorMap*advTurq:                           Texte turquoise

ColorMap*advWhite:                          Texte blanc

ColorMap*advYellow:                         Texte jaune

ColorMap*advCursor:                         Curseur

ColorMap*advMouse:                          Pointeur de la souris

ColorMap*advRule:                           R�gle

ColorMap*advStatLineBack:                   Arri�re-plan ligne d'�tat

ColorMap*advStatLineFore:                   1e plan ligne d'�tat

ColorMap*advMonoImageBack:                  Arri�re-plan img monochrome

ColorMap*advMonoImageFore:                  1er plan img monochrome

! Note: foreground color used for assist program windows
ColorMap*advAssistFore:                     1er plan prog assistance

! Note: button background for assist program windows
ColorMap*advAssistForm:                     Bouton prog assistance

! Note: main background color for assist program windows
ColorMap*advAssistPrimary:                  Clr principale prog assist

! Note: entry area background color for assist program windows
ColorMap*advAssistEntry:                    Zone d'entr�e prog assist


ColorMap*red.titleString:                   ROUGE 

ColorMap*green.titleString:                 VERT

ColorMap*blue.titleString:                  BLEU 

ColorMap*currentColorLabel.labelString:     Couleur en cours

ColorMap*applyCurrentColor.labelString:     Appliquer la couleur en cours


!************************************
!**  Delete file dialog            **
!************************************
ColorMap*deleteDialog.deleteBoxDeleteBtn.labelString:  Suppression

ColorMap*deleteDialog.deleteBoxCancelBtn.labelString:  Annulation

! title of delete dialog
ColorMap*deleteDialog.dialogTitle:                     Suppression de fichier

ColorMap*deleteDialog.deleteBoxLabel.labelString:      S�lectionnez le ou les fichiers � supprimer :

ColorMap*deleteDialog.deleteBoxLabelAlt.labelString:   Aucun fichier � supprimer.

! message at bottom of primary window
ColorMap*selectedDeleted:                              Le ou les fichiers de d�finition de couleurs s�lectionn�s ont �t� supprim�s.


!*****************************************************
!**  Delete file verification dialog box            **
!*****************************************************
ColorMap*deleteVerifyDialog.dialogTitle:        Confirmation de suppression

ColorMap*deleteVerifyDialog.okLabelString:      OK

ColorMap*deleteVerifyDialog.cancelLabelString:  Annulation

! this cannot be much longer
ColorMap*noUndoDelete:                          La suppression des fichiers est irr�versible.\nSouhaitez-vous continuer ?

ColorMap*noFilesDelete:                         Aucun fichier n'a �t� s�lectionn�.\nDemande de suppression incorrecte.


!****************************************************
!**  Emulator preference in construct list         **
!**						   **
!**  MAX:  text should be less than 15 characters  **
!****************************************************
ColorMap*emuPrefMenuLabel.labelString:       Barre de menus

ColorMap*emuPrefBlueText.labelString:        Texte bleu

ColorMap*emuPrefGreenText.labelString:       Texte vert

ColorMap*emuPrefPinkText.labelString:        Texte rose

ColorMap*emuPrefRedText.labelString:         Texte rouge

ColorMap*emuPrefTurquoiseText.labelString:   Texte turquoise

ColorMap*emuPrefWhiteText.labelString:       Texte blanc

ColorMap*emuPrefYellowText.labelString:      Texte jaune

ColorMap*emuPrefPbBackground.labelString:    Btn de fonction

ColorMap*emuPrefStatusLine.labelString:      Ligne d'�tat

ColorMap*groupBoxLabel.labelString:          Pgm assistance

ColorMap*entryAreaLabel.labelString:         Zone d'entr�e

ColorMap*helperFormLabel.labelString:        Bouton


!**************************
!**  Error dialog boxes  **
!**************************
ColorMap*noColorCellsMsgText.value: Impossible d'allouer les ressources de couleurs requises pour d�marrer le programme de d�finition de couleurs. Fermez une application utilisant beaucoup de couleurs et recommencez.

ColorMap*setColorMsgText.value:      Toutes les couleurs n'ont pas �t� appliqu�es � la session d'�mulation. Cependant, les couleurs correctes seront sauvegard�es si vous s�lectionnez Sauvegarde.

ColorMap*setColorFailureCancelBtn.labelString:        Annulation

ColorMap*invalidColor:   Les couleurs d'avant-plan et d'arri�re-plan suivantes sont trop proches l'une de l'autre : 


!**********************************
!**  General Error dialog boxes  **
!**********************************
ColorMap*noAdvanced.value:            Impossible d'allouer des ressources de couleurs pour fournir une d�finition de couleurs int�grale.

ColorMap*noAdvancedOkbutton.labelString:  OK


!*******************************************************************************
!**  quitDialog resources                                                     **
!*******************************************************************************
ColorMap*quitDialog.dialogTitle:           Avertissement

ColorMap*quitDialog1:                      Les couleurs de l'�mulateur ont �t� modifi�es,\n\
mais elles n'ont pas �t� appliqu�es ni sauvegard�es.

ColorMap*quitDialog2:                      Les couleurs de l'�mulateur ont �t� modifi�es et appliqu�es,\n\
mais elles n'ont pas �t� sauvegard�es.

ColorMap*quitDialog3:                      Les couleurs de l'�mulateur ont �t� modifi�es et sauvegard�es,\n\
mais elles n'ont pas �t� appliqu�es.

ColorMap*quitDialog*exitBox.labelString:   Sortie

ColorMap*quitDialog*cancelBox.labelString: Annulation

ColorMap*quit1Tog1:                        Quitter sans appliquer ni sauvegarder les modifications.

ColorMap*quit2Tog1:                        Quitter sans sauvegarder les modifications.

ColorMap*quitDialog*quitTog2.labelString:  Appliquer et sauvegarder les modifications avant de quitter.

ColorMap*quit3Tog1:                        Quitter sans appliquer les modifications.

ColorMap*quitTog3.labelString:             Appliquer les modifications � cette session avant de quitter.

ColorMap*quitDialog*quitTog4.labelString:  Sauvegarder les modifications avant de quitter.


!*******************************************************************************
!**  Informational Messages                                                   **
!*******************************************************************************
ColorMap*InfoMsgColorsSaved:    Couleurs sauvegard�es dans le fichier : 

ColorMap*tempApp:               Les couleurs ont �t� appliqu�es provisoirement � la session d'�mulation.

ColorMap*notLoaded:             Impossible de charger le fichier de couleurs. V�rifiez qu'il existe toujours.

ColorMap*errorLoad:             Le format du fichier de couleurs est incorrect.


! this resource should contain one black space character; do not translate.
ColorMap*blank:  

ColorMap*chooseApply:           Cliquez sur Appliquer pour que les couleurs soient prises en compte dans la session.

ColorMap*noAccessDir:           Impossible d'acc�der au r�pertoire.

! this cannot be made any longer during translation
ColorMap*noOpenFile:            Impossible d'ouvrir le fichier de d�finition de couleurs : 

ColorMap*noMakeDir:             Impossible de cr�er le r�pertoire.

ColorMap*noDeterminePath:       Impossible de d�terminer le chemin d'acc�s au fichier.
