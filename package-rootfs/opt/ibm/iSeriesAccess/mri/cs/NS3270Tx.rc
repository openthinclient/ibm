!
!
!
! This file contains the resource text strings for the IBM Linux 3270 and 5250
! emulators, in particular the main emulator windows.

! This resource file will be stored on all servers in locale specific ASCII (819 for Latin1
! languages). 

! Resource file format:
! 1. Standard ASCII text file.
! 2. Comment lines are preceded by a "!" character in column 1 (these lines not translated).
! 3. This resource file contains text strings for the 3270 and 5250 emulators.  Much of
!    the resource text is common to both emulators. Some is specific to one emulator.
!    IBM9999 = Resources used by both 3270 and 5250 emulators.
!    IBM3270 = Resources used by the 3270 emulator.
!    IBM5250 = Resources used by the 5250 emulator.
! 4. Resource format: 
!    - A resource line starts with IBM3270*, IBM5250*, or IBM9999*, followed by a resource name
!      and a ":".  Do NOT make any changes to the text to the left of the first ":"
!    - Text to the right of the first ":" on a line is translated.

!******************************************************************************
!************************ BEGIN DO NOT TRANSLATE ******************************
!******************************************************************************
!*   The first section of resources are are locale specific but               *
!*   DO NOT REQUIRE TRANSLATION.                                              *
!*                                                                            *
!* This source file enables the resource section for Latin1 and Latin2.       *
!*                                                                            *
!* For ru_RU or Latin5 locales and DBCS locales                               *
!*      1. Place a '!' as the first character in each line in the Latin1      *
!*      and Latin2 section                                                    *
!*      2. Remove the '!' character for each resource line in the apropriate  *
!*      section that matches your locale.                                     *
!*                                                                            *
!* For example, ru_RU should place ! in front of all lines in the LATIN1      *
!* section remove the ! from all lines in the LATIN5 section.                 *      
!******************************************************************************

!**************************************************************************
!****************   BEGIN LATIN1 and LATIN2                ****************
!**************************************************************************
IBM9999*font_List:     -*-*-medium-r-normal-*-*-*-*-*-m-150-*-*;-*-*-medium-r-normal-*-*-*-*-*-m-110-*-*;-*-*-medium-r-normal-*-*-*-*-*-m-90-*-*;-*-*-medium-r-normal-*-*-*-*-*-m-70-*-*;-*-*-medium-r-normal-*-*-*-*-*-m-60-*-*
IBM9999*largestFontList:     -*-*-medium-r-normal-*-*-*-*-*-m-150-*-*
IBM9999*extraLargeFontList:  -*-*-medium-r-normal-*-*-*-*-*-m-110-*-*
IBM9999*largeFontList:       -*-*-medium-r-normal-*-*-*-*-*-m-90-*-*
IBM9999*mediumFontList:      -*-*-medium-r-normal-*-*-*-*-*-m-70-*-*
IBM9999*smallFontList:       -*-*-medium-r-normal-*-*-*-*-*-m-60-*-*
!**************************************************************************
!****************   END LATIN1 and LATIN2                  ****************
!**************************************************************************

!**************************************************************************
!****************   BEGIN LATIN5                           ****************
!**************************************************************************
!IBM9999*font_List:     -*-*-medium-r-normal-*-*-*-*-*-c-100-*-*;-*-*-medium-r-normal-*-*-*-*-*-c-90-*-*;-*-*-medium-r-normal-*-*-*-*-*-c-70-*-*;-*-*-medium-r-normal-*-*-*-*-*-c-60-*-*
!IBM9999*largestFontList:     -*-*-medium-r-normal-*-*-*-*-*-c-100-*-*
!IBM9999*extraLargeFontList:  -*-*-medium-r-normal-*-*-*-*-*-c-100-*-*
!IBM9999*largeFontList:       -*-*-medium-r-normal-*-*-*-*-*-c-90-*-*
!IBM9999*mediumFontList:      -*-*-medium-r-normal-*-*-*-*-*-c-70-*-*
!IBM9999*smallFontList:       -*-*-medium-r-normal-*-*-*-*-*-c-60-*-*
!**************************************************************************
!****************   END   LATIN5                           ****************
!**************************************************************************

!**************************************************************************
!****************   BEGIN DBCS (ja_JP ko_KR zh_CN)   ****************
!**************************************************************************
!IBM9999*font_List:            -*-*-medium-r-normal--24-*-*-*-c-*;-*-*-medium-r-normal--16-*-*-*-c-*
!IBM9999*largestFontList:     -*-*-medium-r-normal--24-*-*-*-c-*
!IBM9999*extraLargeFontList:  -*-*-medium-r-normal--24-*-*-*-c-*
!IBM9999*largeFontList:       -*-*-medium-r-normal--16-*-*-*-c-*
!IBM9999*mediumFontList:      -*-*-medium-r-normal--16-*-*-*-c-*
!IBM9999*smallFontList:       -*-*-medium-r-normal--16-*-*-*-c-*
!**************************************************************************
!****************   END DBCS (ja_JP ko_KR zh_CN)     ****************
!**************************************************************************

!**************************************************************************
!****************   BEGIN DBCS (zh_TW)   ****************
!**************************************************************************
!IBM9999*font_List:            -*-*-medium-r-normal--24-*-*-*-c-*,-arphic-ar pl mingti2l big5-medium-r-normal--24-240-0-0-c-*;-*-*-medium-r-normal--16-*-*-*-c-*,-arphic-ar pl mingti2l big5-medium-r-normal--16-160-0-0-c-*
!IBM9999*largestFontList:     -*-*-medium-r-normal--24-*-*-*-c-*,-arphic-ar pl mingti2l big5-medium-r-normal--24-240-0-0-c-*
!IBM9999*extraLargeFontList:  -*-*-medium-r-normal--24-*-*-*-c-*,-arphic-ar pl mingti2l big5-medium-r-normal--24-240-0-0-c-*
!IBM9999*largeFontList:       -*-*-medium-r-normal--16-*-*-*-c-*,-arphic-ar pl mingti2l big5-medium-r-normal--16-160-0-0-c-*
!IBM9999*mediumFontList:      -*-*-medium-r-normal--16-*-*-*-c-*,-arphic-ar pl mingti2l big5-medium-r-normal--16-160-0-0-c-*
!IBM9999*smallFontList:       -*-*-medium-r-normal--16-*-*-*-c-*,-arphic-ar pl mingti2l big5-medium-r-normal--16-160-0-0-c-*
!**************************************************************************
!****************   END DBCS (zh_TW)     ****************
!**************************************************************************

!uncomment the appropriate inputMethod
!For LatinX (non DBCS locales) or locales without an input method.
IBM5250*inputMethod:
!
! For ja_JP
!IBM5250*inputMethod: kinput2
!
! For ko_KR 
!IBM5250*inputMethod: Ami
!
!For zh_CN
!IBM5250*inputMethod: xcin-zh_CN
!
!For zh_TW
!IBM5250*inputMethod: xcin-zh_TW

!**************************************************************************
!****************   END DO NOT TRANSLATE                   ****************
!**************************************************************************



IBM5250*MainWindow.title: 5250
IBM3270*MainWindow.title: 3270

IBM5250*errorMsgDialog_popup.title: Chyba 5250
IBM3270*errorMsgDialog_popup.title: Chyba 3270

IBM5250*warningMsgDialog_popup.title: Informace 5250
IBM3270*warningMsgDialog_popup.title: Informace 3270

IBM5250*hostNameDialog_popup.title: Nov� relace 5250
IBM3270*hostNameDialog_popup.title: Nov� relace 3270

IBM5250*hostNameDialog.selectionLabelString: Zadejte nov� n�zev hostitele 5250.
IBM3270*hostNameDialog.selectionLabelString: Zadejte nov� n�zev hostitele 3270.

IBM5250*hostNameDialog.iconName: Nov� relace 5250
IBM3270*hostNameDialog.iconName: Nov� relace 3270

IBM5250*lowMemoryDialog_popup.title: Nedostatek pam�ti 5250
IBM3270*lowMemoryDialog_popup.title: Nedostatek pam�ti 3270

IBM5250*new5250Button.labelString: Nov� 5250...

IBM5250*quitButton.labelString: Ukon�it 5250
IBM3270*quitButton.labelString: Ukon�it 3270

IBM5250*localPrintControls_popup.title: Tisk obrazovky 5250
IBM3270*localPrintControls_popup.title: Tisk obrazovky 3270


!**************************************************************************************
!** Resources in both 5250 and 3270 that do not need separate translations
!**************************************************************************************

!***********************
!**  Various Dialogs  **
!***********************
IBM9999*lowMemoryDialog.messageString: Nedostatek pam�ti k dokon�en� operace

IBM9999*ErrorMsgMaxSessions: Relace nen� povolena. Byl p�ekro�en maxim�ln� po�et relac�.

! -DISPLAY_NAME is not translated.
IBM9999*ErrorMsgInvalidName: Neplatn� specifikace parametru -DISPLAY_NAME.

! \n in the following resource string allows text following \n to appear on a new line.
! \n is not translated. You can move, add, or remove the \n as needed.
IBM9999*ErrorMsgNameInUse: N�zev nebo n�zvy specifikovan� pro parametr -DISPLAY_NAME jsou ji� pou��v�ny.\n\
Administr�tor syst�mu omezil po�et relac�.

! BROWSER is not translated.
IBM9999*ErrorMsgBrowserStart: Prom�nn� prost�ed� BROWSER nen� definov�na, nelze spustit prohl��e�.

IBM9999*ErrorMsgTelnetUndefined: V souboru slu�eb nen� definov�n protokol Telnet.

IBM9999*ErrorMsgUnknownHost: Hostitel je nezn�m�.

IBM9999*ErrorMsgUnableToConnect: Nelze iniciovat p�ipojen�.

IBM9999*ErrorMsgTelnetNegoFailed: Nav�z�n� spojen� pomoc� protokolu Telnet se nezda�ilo.

IBM9999*ErrorMsgRetryButton: Opakovat

! \n in the following resource string allows text following \n to appear on a new line.
IBM9999*ErrorMsgNoHostData: Spojen� s hostitelem bylo p�eru�eno.\n\
Zkuste restartovat relaci emul�toru.

! \n in the following resource string allows text following \n to appear on a new line.
IBM9999*ErrorMsgNoHostData2: Spojen� s hostitelem bylo p�eru�eno.\n\
M� b�t proveden nov� pokus o spu�t�n� relace emul�toru?

IBM9999*ErrorMsgFatalTelnetError: Do�lo k z�va�n� chyb� protokolu Telnet.

IBM9999*ErrorMsgColorMapActive: Aplikace mapy barev je ji� aktivn� v jin� relaci.

IBM9999*ErrorMsgColorMapLowMemory: Nen� k dispozici dostatek pam�ti pro spu�t�n� n�stroje Mapov�n� barev.

IBM9999*ErrorMsgKeyMapActive: Aplikace mapy kl�ves je ji� aktivn� v jin� relaci.

! this cannot be made much longer during translation.
IBM9999*ErrorMsgKeypadActive:  Aplikace P�izp�soben� pomocn� kl�vesnice je ji� aktivn� v jin� relaci.

IBM9999*ErrorMsgKeyMapLowMemory: Nen� k dispozici dostatek pam�ti pro spu�t�n� n�stroje P�emapov�n� kl�vesnice.

IBM9999*ErrorMsgKeypadLowMemory:   Nen� k dispozici dostatek pam�ti pro spu�t�n� n�stroje P�izp�soben� pomocn� kl�vesnice.

IBM9999*ErrorMsgHelpViewLowMemory: Nedostatek pam�ti pro spu�t�n� prohl��e�e n�pov�dy.

IBM9999*ErrorMsgMiscellaneousLowMemory: Nen� k dispozici dostatek pam�ti pro spu�t�n� n�stroje R�zn� p�edvolby.

! this cannot be made any longer during translation.
IBM9999*ErrorMsgNSMServerDownLevel:   Nelze vytvo�it z�znam. Server IBM Network Station Manager nepracuje na spr�vn� �rovni.

IBM9999*ErrorMsgNoPrinters:           Nejsou konfigurov�ny ��dn� tisk�rny. Obra�te se na spr�vce syst�mu.

! \n in the following resource string allows text following \n to appear on a new line.
IBM9999*ErrorMsgNoKeypadSelected: Nen� vybr�n ��dn� soubor.\n\
Vyberte soubor nebo vyberte volbu Zru�it. 

! \n in the following resource string allows text following \n to appear on a new line.
IBM9999*ErrorMsgNoKeypads: Nebyl nalezen ��dn� soubor pomocn� kl�vesnice. Chcete-li vytvo�it pomocnou kl�vesnici\n\
u�ivatele, v nab�dce Volba vyberte volbu P�izp�sobit pomocnou kl�vesnici.

IBM9999*ErrorMsgTooManyKeypads: V jednom okam�iku m��e b�t aktivov�no pouze 10 pomocn�ch kl�vesnic.
	
IBM9999*ErrorMsgFileNameChars: Zadan� znak nen� platn�m znakem n�zvu souboru.

! \n in the following resource string allows text following \n to appear on a new line.
IBM9999*WarningMsgColorNotAvailable: N�kter� barvy emul�toru nejsou k dispozici.\n\
Tyto barvy jsou pro danou relaci upraveny.

IBM9999*WarningMsgColorFileCorrupt: Byla zaznamen�na chyba souboru barev. Byly pou�ity v�choz� barvy.

IBM9999*hostNameDialog*startupPlaybackChoice.labelString: Pou��t �vodn� soubor p�ehr�v�n�

! java ECL that is not supported yet but please translate
IBM9999*runAppletDialog_popup.title: Aplet ECL Java

! java ECL that is not supported yet but please translate
IBM9999*runAppletDialog.selectionLabelString: Zadejte n�zev t��dy.

! java ECL that is not supported yet but please translate
IBM9999*runAppletDialog.iconName: Aplet ECL Java

! java ECL that is not supported yet but please translate
IBM9999*runAppletDialog: Zadejte n�zev t��dy.

! java ECL that is not supported yet but please translate
IBM9999*ErrorMsgUnableToRunApplet: Nelze na��st t��du


!*******************************
!**  Menu items in emulators  **
!*******************************
IBM9999*cmdCascade.labelString: P��kaz

IBM9999*editCascade.labelString: Upravit

IBM9999*pasteButton.labelString: Vlo�it

IBM9999*copyButton.labelString: Kop�rovat

IBM9999*cutButton.labelString: Vyjmout

IBM9999*optCascade.labelString: Volba

IBM9999*fontMenuCascade.labelString: P�sma

IBM9999*pfkeysButton.labelString: Pomocn� kl�vesnice...

IBM9999*keypadCascade.labelString: Pomocn� kl�vesnice

IBM9999*customizeButton.labelString: P�izp�sobit pomocnou kl�vesnici...   

IBM9999*recordButton.labelString: Zaznamenat...

IBM9999*playbackButton.labelString: P�ehr�t...

IBM9999*miscButton.labelString: R�zn� p�edvolby...

IBM9999*colormapButton.labelString: Mapov�n� barev...

IBM9999*keymapButton.labelString: P�emapov�n� kl�vesnice...

! java ECL that is not supported yet but please translate
IBM9999*runAppletButton.labelString: Spustit aplet...

IBM9999*ctlCascade.labelString: Ovl�d�n�

IBM9999*resetButton.labelString: Kl�vesa Reset

IBM9999*sysreqButton.labelString: Kl�vesa System Request

IBM9999*attentionButton.labelString: Kl�vesa Attention

IBM9999*helpkeyButton.labelString: Kl�vesa Help

IBM9999*enterkeyButton.labelString: Kl�vesa Enter

IBM9999*prtCascade.labelString: Tisk

IBM9999*systemButton.labelString: Syst�m

IBM9999*localButton.labelString: Tisk obrazovky...

IBM9999*helpCascade.labelString: N�pov�da

! message on IBM5250 on status line, cannot be made any longer, you can remove some periods.
IBM9999*msgWaitingMsg.labelString: �ekaj�c� zpr�va.....

IBM9999*cursorRowCol.labelString:  01/001

IBM9999*okLabelString: OK

IBM9999*cancelLabelString: Zru�it

!****************************
!** Local Print Resources
!****************************

IBM9999*localPrintPrinterSelectButton.labelString: Vyberte tisk�rnu:

IBM9999*localPrintPrinterSelectControls_popup.title: V�b�r tisk�rny

IBM9999*localPrintPrinterSelectServerDelineator: @

IBM9999*localPrintPrinterSelectLocalDesignator: (Lok�ln�)

IBM9999*localPrintPDTLabel.labelString: Tabulka definice tisk�rny (PDT):

IBM9999*localPrintOtherOptions.labelString: Dal�� volby tisku:

IBM9999*localPrintFormFeedLabel.labelString: Odstr�nkovat:

IBM9999*localPrintFormFeedYesButton.labelString: Ano

IBM9999*localPrintFormFeedNoButton.labelString: Ne

IBM9999*localPrintOrientationLabel.labelString: Orientace:

IBM9999*localPrintOrientationPortraitButton.labelString: Na v��ku

IBM9999*localPrintOrientationLandscapeButton.labelString: Na ���ku

IBM9999*localPrintControls*startBtn.labelString: Tisk

IBM9999*localPrintControls*cancelBtn.labelString: Zru�it

IBM9999*localPrintControls*selectOptions.labelString: Vyberte volby tisku

IBM9999*localPrintControls*printing.labelString: Prob�h� tisk...

IBM9999*PrintCommand.labelString: Tiskov� p��kaz


!********************************************************
!**                  Record Resources
!**
!** MAX:  Push buttons should not exceed 8 characters
!********************************************************
IBM9999*recordControl_popup.title: Ovl�dac� prvky z�znamu

IBM9999*recordControl*startBtn.labelString: Spustit

IBM9999*recordControl*cancelBtn.labelString: Zru�it

IBM9999*recordControl*pauseBtn.labelString: Pozastavit

IBM9999*recordControl*stopBtn.labelString: Zastavit

IBM9999*recordControl*recordControlLabelRecording.labelString: Prob�h� z�znam.

IBM9999*recordControl*recordControlLabelPaused.labelString: Pozastaveno.

IBM9999*recordControl*recordControlKeysRemaingLabel.labelString: Zb�vaj�c� m�sto:

IBM9999*recordPauseControls_popup.title: Volby pozastaven� z�znamu

! this cannot be made much longer during translation. 
IBM9999*recordPauseControls*recordPauseChoicesInstructions.labelString: Z�znam je pozastaven. Vyberte, jak� akce se m� st�t\n\
b�hem p�ehr�v�n�:

IBM9999*recordPauseControls*recordPauseRecordBtn.labelString: Pozastavit na tomto m�st� p�ehr�v�n�

IBM9999*recordPauseControls*recordPauseUseridBtn.labelString: Vlo�it na tomto m�st� ID u�ivatele

IBM9999*recordPauseControls*recordPausePasswordBtn.labelString: Vlo�it na tomto m�st� heslo

IBM9999*recordPauseControls*recordPauseNoneBtn.labelString: ��dn�

IBM9999*recordPauseControls*continueBtn.labelString:   Pokra�ovat v z�znamu

IBM9999*recordPauseControls*pauseStopBtn.labelString: Zastavit z�znam

IBM9999*recordFileControl_popup.title: Ovl�dac� prvky ulo�en� souboru p�ehr�v�n�

IBM9999*recordFileControl*recordFileListLabel.labelString: Vyberte nahrazovan� soubor p�ehr�v�n�.

IBM9999*recordFileControl*acceleratorBtn.labelString: P�i�adit nepovinnou akcelera�n� kl�vesu

IBM9999*recordFileControl*accButton0.labelString: ��dn�

! Function keys for accelerators.
IBM9999*recordFileControl*accButton1.labelString:  F1

IBM9999*recordFileControl*accButton2.labelString:  F2

IBM9999*recordFileControl*accButton3.labelString:  F3

IBM9999*recordFileControl*accButton4.labelString:  F4

IBM9999*recordFileControl*accButton5.labelString:  F5

IBM9999*recordFileControl*accButton6.labelString:  F6

IBM9999*recordFileControl*accButton7.labelString:  F7

IBM9999*recordFileControl*accButton8.labelString:  F8

IBM9999*recordFileControl*accButton9.labelString:  F9

IBM9999*recordFileControl*accButton10.labelString:  F10

IBM9999*recordFileControl*accButton11.labelString:  F11

IBM9999*recordFileControl*accButton12.labelString:  F12

IBM9999*recordFileControl*accButton13.labelString:  F13

IBM9999*recordFileControl*accButton14.labelString:  F14

IBM9999*recordFileControl*accButton15.labelString:  F15

IBM9999*recordFileControl*accButton16.labelString:  F16

IBM9999*recordFileControl*accButton17.labelString:  F17

IBM9999*recordFileControl*accButton18.labelString:  F18

IBM9999*recordFileControl*accButton19.labelString:  F19

IBM9999*recordFileControl*accButton20.labelString:  F20

IBM9999*recordFileControl*accButton21.labelString:  F21

IBM9999*recordFileControl*accButton22.labelString:  F22

IBM9999*recordFileControl*accButton23.labelString:  F23

IBM9999*recordFileControl*accButton24.labelString:  F24

IBM9999*recordFileControl*recordFileTextLabel.labelString: Zadejte nov� n�zev souboru p�ehr�v�n�.

IBM9999*recordFileControl*saveBtn.labelString: Ulo�it

IBM9999*recordFileControl*doNotSaveBtn.labelString: Neukl�dat

IBM9999*recordFileControl*recordFileReplaceWarningDialog_popup.title: Varov�n� p�i nahrazen� souboru p�ehr�v�n�

IBM9999*recordFileControl*recordFileReplaceWarningDialog.messageString: Soubor ji� existuje. Chcete jej p�epsat?

IBM9999*recordFileControl*recordFileMsgDialog_popup.title: Chybov� zpr�va p�i ukl�d�n� souboru p�ehr�v�n�

IBM9999*RecordFileMsgNoOpen: Soubor nelze otev��t.  Kl�vesov� �hozy nebyly ulo�eny.

IBM9999*RecordFileMsgNoAccess: Adres�� je nedostupn�.

IBM9999*RecordFileMsgNoMake: Nelze vytvo�it adres��.


!****************************
!** Playback Resources
!****************************
IBM9999*playbackControl_popup.title: Ovl�dac� prvky p�ehr�v�n�

IBM9999*playbackControl*playbackControlLabelSelectFile.labelString: Vyberte p�ehr�van� soubor p�ehr�v�n�.

IBM9999*playbackControl*playbackFileListLabel.labelString: U�ivatelsk� soubor p�ehr�v�n�

IBM9999*playbackControl*playbackDefaultFileListLabel.labelString: V�choz� soubor p�ehr�v�n�

IBM9999*scaleBtn.titleString: Rychlost p�ehr�v�n�

IBM9999*playbackControl*startBtn.labelString: Spustit

IBM9999*playbackControl*cancelBtn.labelString: Zru�it

IBM9999*playbackControl*deleteBtn.labelString: Odstranit

IBM9999*playbackControl*editBtn.labelString: Upravit

IBM9999*playbackControl*pauseBtn.labelString: Pozastavit

IBM9999*playbackControl*continueBtn.labelString: Pokra�ovat

IBM9999*playbackControl*stopBtn.labelString: Zastavit

IBM9999*playbackControl*playbackControlLabelStopped.labelString: Zastaveno.

IBM9999*playbackControl*playbackControlLabelPlaying.labelString: Prob�h� p�ehr�v�n�.

IBM9999*playbackControl*playbackControlLabelPaused.labelString:  Pozastaveno.

IBM9999*playbackControl*playbackControlLabelDone.labelString: Dokon�eno.

IBM9999*playbackControl*playbackKeysRemainingLabel.labelString:  Zb�vaj�c� data:

IBM9999*playbackControl*playbackDeleteWarningDialog_popup.title: Varov�n� p�i odstran�n� souboru p�ehr�v�n�

IBM9999*playbackControl*playbackDeleteWarningDialog.messageString: Chcete odstranit vybran� soubory?

IBM9999*playbackControl*playbackMsgDialog_popup.title: Chybov� zpr�va p�i p�ehr�v�n�

IBM9999*playbackControl*playbackPauseDialog_popup.title: P�ehr�v�n� pozastaveno

IBM9999*playbackControl*playbackPauseDialog.okLabelString:        Pokra�ovat

IBM9999*playbackControl*playbackPauseDialog.cancelLabelString:    Zastavit

IBM9999*playbackPauseDialogMsg1: P�ehr�v�n� je pozastaveno. Zadejte data a pokra�ujte.

IBM9999*playbackPauseDialogMsg2: P�ehr�v�n� je pozastaveno. Zadejte heslo a pokra�ujte.

IBM9999*playbackPauseDialogMsg3: P�ehr�v�n� bylo pozastaveno u�ivatelem.

IBM9999*PlaybackMsgNoStartup: �vodn� soubor p�ehr�v�n� nen� v seznamu p�ehr�v�n�.

IBM9999*PlaybackMsgFileNotInList:  Soubor p�ehr�v�n� nen� v seznamu p�ehr�v�n�.

IBM9999*PlaybackMsgFileCorrupt: Soubor p�ehr�v�n� nebyl vytvo�en pomoc� funkce z�znamu.

IBM9999*PlaybackMsgFileNoRead: Nelze ��st kl��ov� soubor p�ehr�v�n�. P�ehr�v�n� nelze spustit.

IBM9999*PlaybackMsgFileNoOpen: Soubor p�ehr�v�n� nelze otev��t. P�ehr�v�n� nelze spustit.

IBM9999*PlaybackMsgIncompatible:  Soubor p�ehr�v�n� nen� kompatibiln� s aktu�ln� obrazovkou.

IBM9999*PlaybackMsgPasswordLocation:  Heslo nelze zadat na pozici kurzoru.


!*****************************
! Keypad Resources
!*****************************
IBM9999*keypadfilechangebox_popup.title:   Rozev�rac� pomocn� kl�vesnice

IBM9999*keypadfilechangeboxlabel.labelString:   Vyberte soubor pomocn� kl�vesnice:

IBM9999*userKeypads.labelString:  U�ivatelsk� pomocn� kl�vesnice

IBM9999*defaultKeypads.labelString:  V�choz� pomocn� kl�vesnice

IBM9999*cancelkeypad.labelString:   Zru�it

IBM9999*keypadOKbox.labelString:   OK

!*****************************************************************************
! This menubar resource is used for double byte languages, but DOES need to be
! translated into all MRI languages (because users can have a different MRI
! language than their emulator session language).
!*****************************************************************************
IBM9999*altviewButton.labelString:  Alternativn� zobrazen�

!***********************************************************
! The resources below are used to prompt the user to choose between two types 
! of Japanese.  These resources should be translated into all languages.
!***********************************************************
IBM9999*hostNameDialog.helpLabelString:  Roz���en�...
IBM9999*katawidget.title:   V�b�r k�dov� str�nky hostitele pro japon�tinu
IBM9999*labelw.labelString:  Vybrat k�dovou str�nku hostitele
IBM9999*button_two.labelString:  Roz���en� katakana (5026)
IBM9999*button_one.labelString:  Roz���en� latinka (5035)
IBM9999*okw.labelString:  OK
IBM9999*cancelw.labelString:  Zru�it


!***********************************
!* Shared Emulator Print Resources *
!***********************************

IBM9999*localPrintPrintSelectionLabel.labelString:       V�b�r tisku:

IBM9999*localPrintPrintSelectionTextButton.labelString:  Text

IBM9999*localPrintPrintSelectionImageButton.labelString: Obr�zek

IBM9999*localPrintImageColorSupportLabel.labelString: Barevn� tisk:

IBM9999*localPrintImageColorSupportFalseButton.labelString: Ne

IBM9999*localPrintImageColorSupportTrueButton.labelString: Ano

IBM9999*localPrintPageSizeMenuLabel.labelString:  Velikost str�nky:

! The following are printer page sizes.
IBM9999*localPrintPageSizeMenu*pageSize1.labelString:  Letter (8,5 x 11 palc�)

IBM9999*localPrintPageSizeMenu*pageSize2.labelString:  Legal (8,5 x 14 palc�)

IBM9999*localPrintPageSizeMenu*pageSize3.labelString:  A4 (210 x 297 mm)

IBM9999*localPrintPageSizeMenu*pageSize4.labelString:  A5 (148 x 210 mm)

IBM9999*localPrintPageSizeMenu*pageSize5.labelString:  B5 ISO (176 x 250 mm)

IBM9999*localPrintPageSizeMenu*pageSize6.labelString:  B5 JIS (182 X 257 mm)

IBM9999*localPrintPageSizeMenu*pageSize7.labelString:  Executive (7,25 x 10,5 palc�)

IBM9999*localPrintPageSizeMenu*pageSize8.labelString:  Statement (8,5 x 5,5 palc�)

IBM9999*localPrintDPIMenuLabel.labelString:  Rozli�en� (dpi):

IBM9999*localPrintDPIMenu*DPI1.labelString:  75

IBM9999*localPrintDPIMenu*DPI2.labelString:  100

IBM9999*localPrintDPIMenu*DPI3.labelString:  150

IBM9999*localPrintDPIMenu*DPI4.labelString:  200

IBM9999*localPrintDPIMenu*DPI5.labelString:  300

IBM9999*localPrintDPIMenu*DPI6.labelString:  600

IBM9999*ErrorMsgBrowserLowMemory:  Nedostatek pam�ti k dokon�en� operace

IBM9999*userKeypads.labelString: U�ivatelsk� pomocn� kl�vesnice

IBM9999*defaultKeypads.labelString: V�choz� pomocn� kl�vesnice

IBM9999*playbackControl*playbackDeleteWarningDialog.cancelLabelString: Zru�it

IBM9999*playbackControl*playbackDeleteWarningDialog.okLabelString: OK

!***********************************
!**  Resources only used by 3270  **
!***********************************
! 'Command' menu
IBM3270*sessMenuCascade.labelString: Nov� relace 3270

IBM3270*new_sessionButton.labelString: Nov� relace 3270

IBM3270*new_mod2Button.labelString: Nov� relace 24x80

IBM3270*new_mod3Button.labelString: Nov� relace 32x80

IBM3270*new_mod4Button.labelString: Nov� relace 43x80

IBM3270*new_mod5Button.labelString: Nov� relace 27x132

IBM3270*graphicsMsg.labelString: Grafika

IBM3270*font0Button.labelString: V�choz�

IBM3270*printTelnetFailure: Tisk\n\
pomoc� protokolu Telnet selhal.

!**************************************************
!*
!*        Printing translations 
!*
!*      Added by AJMEYER for V310.nc
!*
!**************************************************

IBM9999*printDialogPrinterTypeLabel.labelString:  Typ tisk�rny:

IBM9999*printDialogPrinterTypeMenu*postscript.labelString:  PostScript

IBM9999*printDialogPrinterTypeMenu*ascii.labelString:  ASCII

IBM9999*printDialogPrinterTypeMenu*pcl.labelString:  PCL

IBM9999*printDialogPrinterSelectControls_popup.title:   V�b�r tisk�rny

IBM9999*printSelectDialogOKButton.labelString:  OK

IBM9999*printSelectDialogCancelButton.labelString:  Zru�it

IBM9999*printDialogSelectPrinter.labelString: Vyberte tisk�rnu:

IBM9999*printDialogPrintCommandLabel.labelString: Tiskov� p��kaz:

IBM5250*hostNameDialog*useridLabel.labelString: ID u�ivatele:

IBM5250*hostNameDialog*passwordLabel.labelString: Heslo:

!**************************************************
!*
!*                ICU path  
!*
!*     
!**************************************************


IBM5250*ICUTablePath:  /usr/local/share/icu/3.4
! ENGL1SH_VERS10N 1.8.2.1 DO NOT REMOVE OR CHANGE THIS LINE
