//// ENGLISH_SOURCE_VERSION= 1.2 
////This file contains the playback editor program on-line help text for the IBM 3270 and 5250 emulators.
////This help text file will be stored in locale specific ASCII (code page 819 for Latin1 languages). 

////Help text format (this section is not displayed to IBM emulator users):
//// 1. Standard ASCII text file. Each paragraph is one line of ASCII text.
//// 2. Comment lines are preceded by four "/" characters. The first "/" character must be in column 1.
//// 3. This help file contains help text for the 3270 and 5250 emulators. There are some help text differences. The emulator help viewer program will display the appropriate help text for the appropriate emulator. A comment line of four "/" characters followed by:
//// 3A. "3270" indicates subsequent data is only displayed in 3270 on-line help.
//// 3B.  "5250" indicates subsequent data is only displayed in 5250 on-line help.
//// 3C. "9999" indicates subsequent data is displayed in both 3270 and 5250 on-line help. A comment line of "9999" is not required at the beginning of the file, this is the assumed initial state.
//// 3D Except for comment lines, there should be no "5250" in this file. The emulator help viewer program will convert all "3270" to "5250" when displaying this on-line help text in a 5250 emulator.
//// 4. Exactly one help window title must appear in the help text and is preceded by four "-" characters.  The first "-" character must be in column 1. The window title text must appear within double quotes. All text before the window title (this text) is not displayed to the user.
//// 5. Each topic is preceded by four "+" characters. The first "+" character must be in column 1. The topic text must appear within double quotes. The first topic follows the window title.
//// 6. Do not change the order of topics in this help text file.
//// 7. Only one space should be used between sentences (otherwise a line starting a new sentence within a paragraph will appear indented when displayed).

////If the help text file is being translated, all text below this point should be translated, except for comment lines which indicate specific parameters which should not be translated.


----"3270 播放編輯器說明"
++++"一般資訊"
如需使用這個說明檢視程式的資訊，請參閱主要模擬器說明的第一個主題 (選取模擬器視窗中的「說明」功能表列選項)。

這個說明資訊說明如何編輯現有的播放檔案。播放檔案是使用「選項」功能表的「記錄」選項所建立，且由使用者定義的各種不同動作和鍵順序所組成。

附註：一次只能啟動一個播放編輯器程式。

++++"視窗佈置"
播放編輯器視窗具有鍵盤動作選項區域 (左側)、播放動作清單工作區域 (右側)、動作列區域 (底端) 及訊息區域 (底端)。

「鍵盤動作選項區域」包含下列各項：

1)「可用的動作清單」就是可用的鍵盤動作。在這個清單中選取的動作會成為現行動作。

2)「字元清單」會列出所有可用的字元。已選取的字元會成為現行動作。若要選取字元，請按一下字元。

3)「動作說明」按鈕可在動作說明視窗中，提供鍵盤動作定義。您可以列印動作定義。

4)「插入動作 ->」按鈕可將現行動作插入已選取的播放檔案動作上方的播放動作清單工作區域。

附註：只有一個動作可以成為現行動作。


「播放動作清單工作區域」包含下列各項：

1)「現行播放檔案」會顯示播放檔名。

2)「現行播放檔案動作」區域會列出現行播放檔案中的鍵盤動作。您必須至少選取一個鍵盤動作行，才能插入、編輯或移除動作。動作行限制為大約 100 個字元。

3)「編輯動作」按鈕可讓您編輯已選取的動作。編輯視窗中的「確定」按鈕可置換已編輯的動作。「取消」按鈕可取消對於動作所進行的變更。

4)「插入行」按鈕可在播放檔案動作清單中，將空白行插入已選取的位置，然後就可以進行編輯。

5)「移除動作」按鈕可疑除已選取的動作。

6)「確定」按鈕可接受對於播放檔案動作清單所進行的變更。

7)「取消」按鈕可取消對於播放檔案動作清單所進行的變更。


「動作列」按鈕可提供下列功能：

1)「儲存」可讓您儲存播放檔案的變更。您可以變更原始播放檔案或在新播放檔案中儲存變更。

2)「結束」可結束「播放檔案編輯器」程式。

3)「說明」可顯示這個說明本文。


訊息區域：可顯示資訊及錯誤訊息。

++++"滑鼠動作"
「可用的動作清單」會列出可用於播放檔案中的所有可用動作，包括可在播放檔案中記錄的滑鼠動作。滑鼠動作具有由逗點分隔的列和直欄位置參數；例如 (12,61)。

////3270
++++"3270 過早鍵盤解除鎖定"
附註：3270 應用程式有時會在完成螢幕更新之前解除鎖定鍵盤。3270 模擬器會在播放期間自動新增延遲。請參閱主要 3270 說明的「播放」主題，以取得詳細資訊。
////9999
