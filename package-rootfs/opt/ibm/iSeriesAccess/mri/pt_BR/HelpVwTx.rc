! ENGLISH_SOURCE_VERSION= 1.3.1.1 
!
!
!
! This file contains the resource text strings for the IBM Network Station 3270, 5250 and
! VTxxx emulators, in particular the help viewer text; for example pushbutton text. This file
! does not contain the emulator on-line help text; help text is in a different file.

! This resource file will be stored on all servers in locale specific ASCII (819 for Latin1
! languages). 

! Resource file format:
! 1. Standard ASCII text file.
! 2. Comment lines are preceded by a "!" character in column 1 (these lines not translated).
! 3. Resource format: 
!    - A resource line starts with HelpView* followed by a resource name
!      and a ":".  Do NOT make any changes to the text to the left of the first ":"
!    - Text to the right of the first ":" on a line is translated.

!******************************************************************************
!************************ BEGIN DO NOT TRANSLATE ******************************
!******************************************************************************
!*   The first section of resources are are locale specific but               *
!*   DO NOT REQUIRE TRANSLATION.                                              *
!*                                                                            *
!* This source file enables the resource section for Latin1 and Latin2.       *
!*                                                                            *
!* For ru_RU or Latin5 locales and DBCS locales                               *
!*      1. Place a '!' as the first character in each line in the Latin1      *
!*      and Latin2 section                                                    *
!*      2. Remove the '!' character for each resource line in the apropriate  *
!*      section that matches your locale.                                     *
!*                                                                            *
!* For example, ru_RU should place ! in front of all lines in the LATIN1      *
!* section remove the ! from all lines in the LATIN5 section.                 *      
!******************************************************************************

!**************************************************************************
!****************   BEGIN LATIN1 and LATIN2                ****************
!**************************************************************************
HelpView*useExtraLargeFont:   True
HelpView*largestFontList:     -*-*-medium-r-normal-*-*-*-*-*-m-150-*-*
HelpView*extraLargeFontList:  -*-*-medium-r-normal-*-*-*-*-*-m-110-*-*
HelpView*largeFontList:       -*-*-medium-r-normal-*-*-*-*-*-m-90-*-*
HelpView*mediumFontList:      -*-*-medium-r-normal-*-*-*-*-*-m-70-*-*
HelpView*smallFontList:       -*-*-medium-r-normal-*-*-*-*-*-m-60-*-*
HelpView*largestFontColumns:     30
HelpView*extraLargeFontColumns:  40
HelpView*largeFontColumns:       50
HelpView*mediumFontColumns:      64
HelpView*smallFontColumns:       75
!**************************************************************************
!****************   END LATIN1 and LATIN2                  ****************
!**************************************************************************

!**************************************************************************
!****************   BEGIN LATIN5                           ****************
!**************************************************************************
!HelpView*useExtraLargeFont:   True
!HelpView*largestFontList:     -*-*-medium-r-normal-*-*-*-*-*-c-100-*-*
!HelpView*extraLargeFontList:  -*-*-medium-r-normal-*-*-*-*-*-c-100-*-*
!HelpView*largeFontList:       -*-*-medium-r-normal-*-*-*-*-*-c-90-*-*
!HelpView*mediumFontList:      -*-*-medium-r-normal-*-*-*-*-*-c-70-*-*
!HelpView*smallFontList:       -*-*-medium-r-normal-*-*-*-*-*-c-60-*-*
!HelpView*largestFontColumns:     25
!HelpView*extraLargeFontColumns:  25
!HelpView*largeFontColumns:       35
!HelpView*mediumFontColumns:      64
!HelpView*smallFontColumns:       75
!**************************************************************************
!****************   END   LATIN5                           ****************
!**************************************************************************

!**************************************************************************
!****************   BEGIN DBCS (ja_JP ko_KR zh_CN zh_TW)   ****************
!**************************************************************************
!HelpView*useExtraLargeFont:   False
!HelpView*largestFontList:     -*-*-medium-r-normal-*-24-*-*-*-c-*
!HelpView*extraLargeFontList:  -*-*-medium-r-normal-*-24-*-*-*-c-*
!HelpView*largeFontList:       -*-*-medium-r-normal-*-16-*-*-*-c-*
!HelpView*mediumFontList:      -*-*-medium-r-normal-*-16-*-*-*-c-*
!HelpView*smallFontList:       -*-*-medium-r-normal-*-16-*-*-*-c-*
!HelpView*largestFontColumns:     30
!HelpView*extraLargeFontColumns:  40
!HelpView*largeFontColumns:       50
!HelpView*mediumFontColumns:      64
!HelpView*smallFontColumns:       75
!**************************************************************************
!****************   END DBCS (ja_JP ko_KR zh_CN zh_TW)     ****************
!**************************************************************************

!**************************************************************************
!****************   END DO NOT TRANSLATE                   ****************
!**************************************************************************


!*******************************************************************************
!*******                                                             ***********
!*******                 Help Viewer Resources                       ***********
!*******                                                             ***********
!*******************************************************************************

! title above list of topics
HelpView*topicTitle:   T�picos

HelpView*okLabelString: OK

HelpView*cancelLabelString: Cancelar

!************************************************
!*              Action bar buttons
!*
!* MAX:  When in largest font, button length 
!*       must be less than 13 characters
!************************************************
HelpView*searchFwd.labelString:    Procurar para Frente

HelpView*searchBack.labelString:   Procurar para Tr�s

HelpView*searchNew.labelString:    Nova Procura

HelpView*goFwdButton.labelString:  Pr�ximo T�pico

HelpView*goBackButton.labelString: T�pico Anterior

HelpView*exitHelp.labelString:     Sair

HelpView*optionMenu.labelString:   Fonte

HelpView*helpTextPrint.labelString:  Imprimir

HelpView*printDialogCancelButton.labelString: Cancelar	

HelpView*printDialogBoxLabel.labelString: Selecione as op��es de impress�o

HelpView*printDialogPrintCommandLabel.labelString: Comando de Impress�o:

HelpView*printDialogSelectPrinter.labelString: Selecione uma Impressora:

HelpView*PrinterSelectServerDelineator: @

HelpView*PrintOptionTopicSelectionLabel.labelString:  O que imprimir:

HelpView*PrintOptionTopicSelectionTopic.labelString:  Apenas este t�pico

HelpView*PrintOptionTopicSelectionAll.labelString:  Todos os t�picos

HelpView*PrinterSelectLocalDesignator: (Local)

HelpView*printDialogPrinterSelectControls_popup.title:   Seletor de Impressora

HelpView*printDialogPrintInProgress.labelString: Imprimindo...

HelpView*printDialogPrinterTypeLabel.labelString:  Tipo de Impressora:

HelpView*printDialogPrinterTypeMenu*postscript.labelString:  PostScript

HelpView*printDialogPrinterTypeMenu*ascii.labelString:  ASCII

HelpView*printDialogPrinterTypeMenu*pcl.labelString:  PCL

IBM9999*PrintCommand.labelString: Comando de Impress�o:

!*******************
!* Dialog boxes
!*******************
! title of dialog boxes
HelpView*searchDialog*dialogTitle: Di�logo Procurar

HelpView*Cancel*labelString: Cancelar

HelpView.searchForward: Procurar para a frente por...

HelpView.searchReverse: Procurar para tr�s por...

HelpView*searchDialog*okLabelString: OK

HelpView*searchDialog*cancelLabelString: Cancelar


!**********************************************
!* Messages at bottom of the screen
!**********************************************

HelpView.located:       foi localizado com �xito...

HelpView.notLocated:    N�O p�de ser localizado... 	

HelpView.noSearchValue: O valor de pesquisa atual n�o foi definido

HelpView.fileError:     O arquivo n�o p�de ser aberto:

!**************************************************
!*
!*        Printing translations 
!*
!*      Added by AJMEYER for V310.nc
!*
!**************************************************

HelpView*printSelectDialogOKButton.labelString:  OK

HelpView*printSelectDialogCancelButton.labelString:  Cancelar
