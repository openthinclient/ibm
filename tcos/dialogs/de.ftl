error-title = Fehler – IBM iSeries Access

error_uncaught = Ein unerwarteter Fehler ist aufgetreten.

    {$error}

    Bitte informieren Sie Ihren Administrator.
