error-title = Error – IBM iSeries Access

error_uncaught = An unexpected error occurred.

    {$error}

    Please contact your administrator.
